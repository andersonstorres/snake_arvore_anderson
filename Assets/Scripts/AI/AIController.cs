﻿using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AIController : MonoBehaviour
    { 
        List<WorldTile> _currentPath = new List<WorldTile>();

        LineRenderer lr;

        #region Unity Functions

        private void Awake()
        {
            lr = GetComponent<LineRenderer>();
        }

        #endregion

        #region Public Functions

        //ask pathfinder for a path passing current and target position
        public void CalculatePath(Vector3 startPosition, Vector3 targetPosition)
        {
            _currentPath = Pathfinding.Instance.GetPath(Vector3Extensions.Flat(startPosition), Vector3Extensions.Flat(targetPosition));

            if (_currentPath == null) return;

            //toggled in DebugHelper on the scene, draws lines based on the current path for the AI
            if (DebugHelper.Instance.debugPath) 
            {
                lr.positionCount = _currentPath.Count;

                for (int i = 0; i < lr.positionCount; i++)
                {
                    lr.SetPosition(i, _currentPath[i].transform.position + (Vector3.up));
                }
            }
        }

        public bool HasPath()
        {
            //checks if there's still a path
            if (_currentPath.Count > 0)
            {
                return true;
            }

            DebugHelper.Instance.Log("No path", gameObject);

            return false;
        }

        public Vector3 GetComponentNextPosition()
        {
            if(_currentPath.Count == 0)
            {
                DebugHelper.Instance.LogError("Path null", gameObject);
            }

            Vector3 pos = _currentPath[0].transform.position;
            _currentPath.RemoveAt(0);

            return pos;
        }

        #endregion

        #region Private Functions

        #endregion
    }
}
