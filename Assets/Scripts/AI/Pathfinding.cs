﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

namespace AI
{
    public class Pathfinding : MonoBehaviour
    {
        public static Pathfinding Instance;

        Grid gridBase;
        public GameObject gridNode;            
        public GameObject nodePrefab;          

        public int scanStartX;
        public int scanFinishX;        
        
        public int scanStartY;
        public int scanFinishY;

        public int gridSizeX;
        public int gridSizeY;

        private List<GameObject> unsortedNodes = new List<GameObject>();  
        public GameObject[,] nodes;
        private int gridBoundX = 0, gridBoundY = 0;    
        
        List<WorldTile> currentPath = new List<WorldTile>();

        #region Unity Functions

        private void Awake()
        {
            Instance = this;
        }

        void Start()
        {
            gridBase = GetComponent<Grid>();

            scanStartX = Mathf.FloorToInt(BlocksSpawner.Instance.topLeft.position.x);
            scanFinishX = Mathf.FloorToInt(BlocksSpawner.Instance.bottomRight.position.x);

            scanStartY = Mathf.FloorToInt(BlocksSpawner.Instance.bottomRight.position.z);
            scanFinishY = Mathf.FloorToInt(BlocksSpawner.Instance.topLeft.position.z);

            gridSizeX = Mathf.Abs(scanStartX) + Mathf.Abs(scanFinishX);
            gridSizeY = Mathf.Abs(scanStartY) + Mathf.Abs(scanFinishY);

            CreateGrid();
        }

        #endregion

        #region Public Functions

        public List<WorldTile> GetNeighbours(int x, int y, int width, int height)
        {
            List<WorldTile> myNeighbours = new List<WorldTile>();

            if (x > 0 && x < width - 1)
            {
                if (y > 0 && y < height - 1)
                {
                    if (nodes[x + 1, y] != null)
                    {
                        WorldTile wt1 = nodes[x + 1, y].GetComponent<WorldTile>();
                        if (wt1 != null) myNeighbours.Add(wt1);
                    }

                    if (nodes[x - 1, y] != null)
                    {
                        WorldTile wt2 = nodes[x - 1, y].GetComponent<WorldTile>();
                        if (wt2 != null) myNeighbours.Add(wt2);
                    }

                    if (nodes[x, y + 1] != null)
                    {
                        WorldTile wt3 = nodes[x, y + 1].GetComponent<WorldTile>();
                        if (wt3 != null) myNeighbours.Add(wt3);
                    }

                    if (nodes[x, y - 1] != null)
                    {
                        WorldTile wt4 = nodes[x, y - 1].GetComponent<WorldTile>();
                        if (wt4 != null) myNeighbours.Add(wt4);
                    }
                }
                else if (y == 0)
                {
                    if (nodes[x + 1, y] != null)
                    {
                        WorldTile wt1 = nodes[x + 1, y].GetComponent<WorldTile>();
                        if (wt1 != null) myNeighbours.Add(wt1);
                    }

                    if (nodes[x - 1, y] != null)
                    {
                        WorldTile wt2 = nodes[x - 1, y].GetComponent<WorldTile>();
                        if (wt2 != null) myNeighbours.Add(wt2);
                    }

                    if (nodes[x, y + 1] == null)
                    {
                        WorldTile wt3 = nodes[x, y + 1].GetComponent<WorldTile>();
                        if (wt3 != null) myNeighbours.Add(wt3);
                    }
                }
                else if (y == height - 1)
                {
                    if (nodes[x, y - 1] != null)
                    {
                        WorldTile wt4 = nodes[x, y - 1].GetComponent<WorldTile>();
                        if (wt4 != null) myNeighbours.Add(wt4);
                    }
                    if (nodes[x + 1, y] != null)
                    {
                        WorldTile wt1 = nodes[x + 1, y].GetComponent<WorldTile>();
                        if (wt1 != null) myNeighbours.Add(wt1);
                    }

                    if (nodes[x - 1, y] != null)
                    {
                        WorldTile wt2 = nodes[x - 1, y].GetComponent<WorldTile>();
                        if (wt2 != null) myNeighbours.Add(wt2);
                    }
                }
            }
            else if (x == 0)
            {
                if (y > 0 && y < height - 1)
                {
                    if (nodes[x + 1, y] != null)
                    {
                        WorldTile wt1 = nodes[x + 1, y].GetComponent<WorldTile>();
                        if (wt1 != null) myNeighbours.Add(wt1);
                    }

                    if (nodes[x, y - 1] != null)
                    {
                        WorldTile wt4 = nodes[x, y - 1].GetComponent<WorldTile>();
                        if (wt4 != null) myNeighbours.Add(wt4);
                    }

                    if (nodes[x, y + 1] != null)
                    {
                        WorldTile wt3 = nodes[x, y + 1].GetComponent<WorldTile>();
                        if (wt3 != null) myNeighbours.Add(wt3);
                    }
                }
                else if (y == 0)
                {
                    if (nodes[x + 1, y] != null)
                    {
                        WorldTile wt1 = nodes[x + 1, y].GetComponent<WorldTile>();
                        if (wt1 != null) myNeighbours.Add(wt1);
                    }

                    if (nodes[x, y + 1] != null)
                    {
                        WorldTile wt3 = nodes[x, y + 1].GetComponent<WorldTile>();
                        if (wt3 != null) myNeighbours.Add(wt3);
                    }
                }
                else if (y == height - 1)
                {
                    if (nodes[x + 1, y] != null)
                    {
                        WorldTile wt1 = nodes[x + 1, y].GetComponent<WorldTile>();
                        if (wt1 != null) myNeighbours.Add(wt1);
                    }

                    if (nodes[x, y - 1] != null)
                    {
                        WorldTile wt4 = nodes[x, y - 1].GetComponent<WorldTile>();
                        if (wt4 != null) myNeighbours.Add(wt4);
                    }
                }
            }
            else if (x == width - 1)
            {
                if (y > 0 && y < height - 1)
                {
                    if (nodes[x - 1, y] != null)
                    {
                        WorldTile wt2 = nodes[x - 1, y].GetComponent<WorldTile>();
                        if (wt2 != null) myNeighbours.Add(wt2);
                    }

                    if (nodes[x, y + 1] != null)
                    {
                        WorldTile wt3 = nodes[x, y + 1].GetComponent<WorldTile>();
                        if (wt3 != null) myNeighbours.Add(wt3);
                    }

                    if (nodes[x, y - 1] != null)
                    {
                        WorldTile wt4 = nodes[x, y - 1].GetComponent<WorldTile>();
                        if (wt4 != null) myNeighbours.Add(wt4);
                    }
                }
                else if (y == 0)
                {
                    if (nodes[x - 1, y] != null)
                    {
                        WorldTile wt2 = nodes[x - 1, y].GetComponent<WorldTile>();
                        if (wt2 != null) myNeighbours.Add(wt2);
                    }
                    if (nodes[x, y + 1] != null)
                    {
                        WorldTile wt3 = nodes[x, y + 1].GetComponent<WorldTile>();
                        if (wt3 != null) myNeighbours.Add(wt3);
                    }
                }
                else if (y == height - 1)
                {
                    if (nodes[x - 1, y] != null)
                    {
                        WorldTile wt2 = nodes[x - 1, y].GetComponent<WorldTile>();
                        if (wt2 != null) myNeighbours.Add(wt2);
                    }

                    if (nodes[x, y - 1] != null)
                    {
                        WorldTile wt4 = nodes[x, y - 1].GetComponent<WorldTile>();
                        if (wt4 != null) myNeighbours.Add(wt4);
                    }
                }
            }

            return myNeighbours;
        }

        public List<WorldTile> GetPath(Vector3 start, Vector3 end)
        {
            FindPath(start, end);
            return currentPath;
        }

        #endregion

        #region Private Functions

        void CreateGrid()
        {
            int gridX = 0, gridY = 0;
            bool foundTileOnLastPass = false;

            for (int x = scanStartX; x <= scanFinishX; x++)
            {
                for (int y = scanStartY; y <= scanFinishY; y++)
                {
                    Vector3 worldPosition = new Vector3(x + gridBase.transform.position.x, 0, y + gridBase.transform.position.z);

                    GameObject node = (GameObject)Instantiate(nodePrefab, worldPosition, Quaternion.Euler(0, 0, 0));
                    Vector3Int cellPosition = Vector3Int.RoundToInt(worldPosition);

                    WorldTile wt = node.GetComponent<WorldTile>();
                    wt.gridX = gridX; wt.gridY = gridY; wt.cellX = cellPosition.x; wt.cellY = cellPosition.y;
                    node.transform.parent = gridNode.transform;

                    foundTileOnLastPass = true;
                    node.name = "Walkable_" + gridX.ToString() + "_" + gridY.ToString();

                    unsortedNodes.Add(node);

                    gridY++;
                    if (gridX > gridBoundX)
                        gridBoundX = gridX;

                    if (gridY > gridBoundY)
                        gridBoundY = gridY;
                }

                if (foundTileOnLastPass)
                {
                    gridX++;
                    gridY = 0;
                    foundTileOnLastPass = false;
                }
            }

            nodes = new GameObject[gridBoundX + 1, gridBoundY + 1];

            foreach (GameObject g in unsortedNodes)
            {
                WorldTile wt = g.GetComponent<WorldTile>();
                nodes[wt.gridX, wt.gridY] = g;
            }

            for (int x = 0; x <= gridBoundX; x++)
            {
                for (int y = 0; y <= gridBoundY; y++)
                {
                    if (nodes[x, y] != null)
                    {
                        WorldTile wt = nodes[x, y].GetComponent<WorldTile>();
                        wt.myNeighbours = GetNeighbours(x, y, gridBoundX, gridBoundY);
                    }
                }
            }
        }

        WorldTile GetWorldTileByPosition(Vector3 worldPosition)
        {
            WorldTile wt = null;

            foreach (GameObject node in unsortedNodes)
            {
                if(node.transform.position == worldPosition)
                {
                    wt = node.GetComponent<WorldTile>();
                }
            }

            return wt;
        }

        int GetDistance(WorldTile nodeA, WorldTile nodeB)
        {
            int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
            int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

            if (dstX > dstY)
                return 14 * dstY + 10 * (dstX - dstY);
            return 14 * dstX + 10 * (dstY - dstX);
        }

        List<WorldTile> RetracePath(WorldTile startNode, WorldTile targetNode)
        {
            List<WorldTile> path = new List<WorldTile>();
            WorldTile currentNode = targetNode;

            while (currentNode != startNode)
            {
                path.Add(currentNode);
                currentNode = currentNode.parent;
            }

            path.Reverse();
            return path;
        }

        void FindPath(Vector3 startPosition, Vector3 endPosition)
        {
            WorldTile startNode = GetWorldTileByPosition(startPosition);
            WorldTile targetNode = GetWorldTileByPosition(endPosition);

            if(startNode == null || targetNode == null)
            {
                DebugHelper.Instance.LogError("Wrong position: start " + startPosition + " ------ end " + endPosition, gameObject);
                return;
            }

            List<WorldTile> openSet = new List<WorldTile>();
            HashSet<WorldTile> closedSet = new HashSet<WorldTile>();
            openSet.Add(startNode);

            while (openSet.Count > 0)
            {
                WorldTile currentNode = openSet[0];
                for (int i = 1; i < openSet.Count; i++)
                {
                    if (openSet[i].fCost < currentNode.fCost || openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost)
                    {
                        currentNode = openSet[i];
                    }
                }

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                if (currentNode == targetNode)
                {
                    currentPath = RetracePath(startNode, targetNode);
                    return;
                }

                if (currentNode == null)
                {
                    DebugHelper.Instance.Log("NODE NULL: start" + startPosition + " end: " + endPosition, gameObject);
                }

                foreach (WorldTile neighbour in currentNode.myNeighbours)
                {
                    if (!neighbour.walkable || closedSet.Contains(neighbour)) continue;

                    int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
                    if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {
                        neighbour.gCost = newMovementCostToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, targetNode);
                        neighbour.parent = currentNode;

                        if (!openSet.Contains(neighbour))
                            openSet.Add(neighbour);
                    }
                }
            }
        }

        #endregion

    }
}
