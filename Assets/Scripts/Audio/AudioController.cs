using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Audio
{
    public class AudioController : MonoBehaviour
    {
        #region Singleton
        private static AudioController _instance;

        public static AudioController Instance { get { return _instance; } }


        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
                Configure();
            }

            DontDestroyOnLoad(this.gameObject);
        }
        #endregion

        public AudioTrack[] tracks;

        Hashtable m_AudioTable; //relationship between audio types (key) and audio tracks (value)
        Hashtable m_JobTable; // relationship between audio types (key) and jobs (value) (Coroutine.IEnumerator)

        Vector3 v3Zero = Vector3.zero;

        [System.Serializable]
        public class AudioObject
        {
            public AudioType type;
            public AudioClip clip;
        }

        [System.Serializable]
        public class AudioTrack
        {
            public AudioSource source;
            public AudioObject[] audio;
        }

        private class AudioJob
        {
            public AudioAction action;
            public AudioType type;
            public bool loop;
            public bool fade;
            public float delay;
            public float spatialBlend;
            public Vector3 pos;

            public AudioJob(AudioAction _action, AudioType _type, bool _loop, bool _fade, float _delay, float _spatialBlend, Vector3 _position)
            {
                action = _action;
                type = _type;
                loop = _loop;
                fade = _fade;
                delay = _delay;
                spatialBlend = _spatialBlend;
                pos = _position;
            }
        }

        private enum AudioAction
        {
            START,
            STOP,
            RESTART
        }


        #region Unity Functions

        private void OnDisable()
        {
            //Dispose();
        }

        #endregion

        #region Public Functions

        public void PlayAudio(
            AudioType _type, 
            bool _loop = false, bool _fade = false, float _delay = 0f)
        {
            AddJob(new AudioJob(AudioAction.START, _type, _loop, _fade, _delay, 0, Vector3.zero));
        }        

        //override to deal with 3D sound
        public void PlayAudio(
            AudioType _type, 
            Vector3 _position, 
            float _spatialBlend = 0f, 
            bool _loop = false, 
            bool _fade = false, 
            float _delay = 0f)
        {
            AddJob(new AudioJob(AudioAction.START, _type, _loop, _fade, _delay, _spatialBlend, _position));
        }

        public void StopAudio(AudioType _type, bool _fade = false, float _delay = 0f)
        {
            AddJob(new AudioJob(AudioAction.STOP, _type, false,_fade, _delay, 0, Vector3.zero));
        }

        public void RestartAudio(AudioType _type, bool _fade = false, float _delay = 0f)
        {
            AddJob(new AudioJob(AudioAction.RESTART, _type, false, _fade, _delay, 0, Vector3.zero));
        }

        #endregion

        #region Private Functions

        void Configure()
        {
            m_AudioTable = new Hashtable();
            m_JobTable = new Hashtable();

            GenerateAudioTable();
        }

        void Dispose()
        {
            foreach (DictionaryEntry _entry in m_JobTable)
            {
                IEnumerator _job = (IEnumerator)_entry.Value;
                StopCoroutine(_job);
            }
        }

        void GenerateAudioTable()
        {
            foreach(AudioTrack _track in tracks)
            {
                foreach(AudioObject _obj in _track.audio)
                {
                    //do not duplicate keys
                    if (m_AudioTable.ContainsKey(_obj.type))
                    {
                        DebugHelper.Instance.LogWarning("You are trying to register audio [" + _obj.type + "] that has already been registered.", gameObject);
                    }
                    else
                    {
                        m_AudioTable.Add(_obj.type, _track);

                        //DebugHelper.Instance.Log("Registering audio [" + _obj.type + "].", gameObject);
                    }
                }
            }
        }

        IEnumerator RunAudioJob(AudioJob _job)
        {
            yield return new WaitForSeconds(_job.delay);

            AudioTrack _track = (AudioTrack)m_AudioTable[_job.type];
            _track.source.clip = GetAudioClipFromAudioTrack(_job.type, _track);

            switch (_job.action)
            {
                case AudioAction.START:

                    _track.source.loop = _job.loop;
                    _track.source.Play();

                    break;
                case AudioAction.STOP:

                    if (!_job.fade)
                    {
                        _track.source.Stop();
                    }

                    break;
                case AudioAction.RESTART:

                    _track.source.Stop();
                    _track.source.Play();

                    break;
            }

            if (_job.fade)
            {
                float _initial = _job.action == AudioAction.START || _job.action == AudioAction.RESTART ? 0f : 1f;
                float _target = _initial == 0 ? 1f : 0f;
                float _duration = 1f;
                float _timer = 0f;

                while(_timer <= _duration)
                {
                    _track.source.volume = Mathf.Lerp(_initial, _target, _timer / _duration);
                    _timer += Time.deltaTime;

                    yield return null;
                }

                if(_job.action == AudioAction.STOP)
                {
                    _track.source.Stop();
                }
            }

            m_JobTable.Remove(_job.type);
            DebugHelper.Instance.Log("Job count: " + m_JobTable.Count, gameObject);

            yield return null;
        }

        void AddJob(AudioJob _job)
        {
            //remove conflicting jobs
            RemoveConflictingJobs(_job.type);

            //start job
            IEnumerator _jobRunner = RunAudioJob(_job);
            m_JobTable.Add(_job.type, _jobRunner);
            StartCoroutine(_jobRunner);
            DebugHelper.Instance.Log("Starting job on [" + _job.type + "] with operation: " + _job.action, gameObject);
        }

        void RemoveJob(AudioType _type)
        {
            if (!m_JobTable.ContainsKey(_type))
            {
                DebugHelper.Instance.Log("Trying to stop a job [" + _type + "] that is not running", gameObject);
                return;
            }

            IEnumerator _runningJob = (IEnumerator) m_JobTable[_type];
            StopCoroutine(_runningJob);
            m_JobTable.Remove(_type);
        }

        void RemoveConflictingJobs(AudioType _type)
        {
            if (m_JobTable.ContainsKey(_type))
            {
                RemoveJob(_type);
            }

            AudioType _conflicAudio = AudioType.NONE;
            foreach(DictionaryEntry _entry in m_JobTable)
            {
                AudioType _audioType = (AudioType)_entry.Key;
                AudioTrack _audioTrackInUse = (AudioTrack) m_AudioTable[_audioType];
                AudioTrack _audioTrackNeeded = (AudioTrack) m_AudioTable[_type];

                if(_audioTrackNeeded.source == _audioTrackInUse.source)
                {
                    //conflict
                    _conflicAudio = _audioType;
                }
            }

            if(_conflicAudio != AudioType.NONE)
            {
                RemoveJob(_conflicAudio);
            }
        }

        private AudioClip GetAudioClipFromAudioTrack(AudioType _type, AudioTrack _track)
        {
           foreach(AudioObject _obj in _track.audio)
           {
                if(_obj.type == _type)
                {
                    return _obj.clip;
                }
           }

            return null;
        }

        #endregion
    }
}
