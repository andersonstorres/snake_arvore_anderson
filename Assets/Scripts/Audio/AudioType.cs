namespace Audio
{
    public enum AudioType
    {
        NONE,
        ST_01,
        UI_REGISTER_KEY,
        UI_BUTTON_CLICK,
        SFX_COLLECT_BLOCK_PLAYER,
        SP_REWIND,
        SFX_EXPLOSION,
        SP_END,
        SFX_SPAWN
         
        //custom audio types here
    }
}