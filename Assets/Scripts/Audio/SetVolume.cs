﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;
using UnityEngine.UI;

namespace Audio
{
    public class SetVolume : MonoBehaviour
    {
        [SerializeField] string mixerName;
        [SerializeField] AudioMixer mixer;
        [SerializeField] TextMeshProUGUI volumePercent;
        [SerializeField] Slider slide;

        #region Unity Functions
        private void Start()
        {
            slide.value = PlayerPrefs.GetFloat(mixerName, 0.5f);
        }

        #endregion

        #region Public Functions

        public void SetLevel(float sliderValue)
        {
            mixer.SetFloat(mixerName, Mathf.Log10(sliderValue) * 20);

            PlayerPrefs.SetFloat(mixerName, sliderValue);

            volumePercent.text = Mathf.CeilToInt((sliderValue * 100f)) + "%";
        }

        #endregion

        #region Private Functions
        #endregion
    }
}
