﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using Snake;

namespace Block
{
    public class BatteringRam : BlockBase
    {
        #region Unity Functions
        #endregion

        #region Public Functions

        public override void ActivatePassiveEffect()
        {
            base.ActivatePassiveEffect();
            snake.GetComponent<SnakeSetupBase>().AddToCountByType(BlockType.BATTERING_RAM);
        }

        #endregion

        #region Private Functions
        #endregion
    }
}
