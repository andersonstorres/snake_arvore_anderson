﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Snake;
using Core;
using Score;
using Audio;
using DG.Tweening;

namespace Block
{
    public enum BlockType
    {
        NONE, ENGINE_POWER, TIME_TRAVEL, BATTERING_RAM
    }

    public class BlockBase : MonoBehaviour
    {
        public Stack<MovementRecord> movementRecords = new Stack<MovementRecord>();
        public BlockType type;     
        public SnakeMovementBase snake;
        public SnakeMovementPlayer originalSnake; //never changes

        [SerializeField] int materialChangeIndex;
        [SerializeField] GameObject cover;
        [SerializeField] MeshRenderer meshRenderer;
        [SerializeField] MeshRenderer meshRendererBG;

        BoxCollider _collider;
        Vector3 _lastTimeTravelPos = new Vector3(-100,-100,-100);
        Quaternion _lastTimeTravelRot;
        bool _head;
        bool _inAnimation;       

        [System.Serializable]
        public class MovementRecord
        {
            public Vector3 regPos;
            public Quaternion regRot;

            public MovementRecord(Vector3 pos, Quaternion rot)
            {
                regPos = pos;
                regRot = rot;
            }
        }

        private void Awake()
        {
            _collider = GetComponent<BoxCollider>();
            _collider.isTrigger = false;

            meshRenderer = GetComponentInChildren<MeshRenderer>();
        }

        #region Unity Functions

        private void OnTriggerEnter(Collider other)
        {
            if (_head)
            {
                if(other.TryGetComponent(out BlockBase otherBlock))
                {
                    SnakeMovementAI snakeAI = GetComponentInParent<SnakeMovementAI>();

                    //player case
                    if (otherBlock.IsFreeBlock() && 
                        snake != null &&
                        snake.gameObject != null &&
                        otherBlock.snake != null &&
                        otherBlock.snake.gameObject != null && 
                        snake.gameObject == otherBlock.snake.gameObject) //player case
                    {
                        DebugHelper.Instance.Log("Player collected block", gameObject);
                        snake.AddBlockToBody(otherBlock.type);

                        snake.GetComponent<SnakeScore>().AddScore();
                        snake.GetComponent<SnakeScore>().AddBlock(otherBlock.type);

                        Destroy(other.gameObject);
                        AudioController.Instance.PlayAudio(Audio.AudioType.SFX_COLLECT_BLOCK_PLAYER);

                        BlocksSpawner.Instance.AskForNewBlock(snake.gameObject);
                        snake.GetComponent<SnakeMovementPlayer>().GetNemesis().GetComponent<SnakeMovementAI>().DecideToRecalculate();
                    }
                    else //AI case
                    if (otherBlock.IsFreeBlock() &&
                        snake != null &&
                        snake.gameObject != null &&
                        otherBlock.snake != null &&
                        otherBlock.snake.gameObject != null &&
                        (snakeAI != null && snakeAI.GetNemesis() == otherBlock.snake.gameObject))
                    {
                        DebugHelper.Instance.Log("AI collected block", gameObject);
                        snake.AddBlockToBody(otherBlock.type);

                        Destroy(other.gameObject);
                        AudioController.Instance.PlayAudio(Audio.AudioType.SFX_COLLECT_BLOCK_PLAYER);

                        BlocksSpawner.Instance.AskForNewBlock(snakeAI.GetNemesis());
                    }
                    else 
                    if (otherBlock.IsFreeBlock())
                    {
                        //if it's another snake's block
                    }
                    else
                    {
                        snake.HandleCollision(this, otherBlock);
                    }
                }
                else
                {
                    DebugHelper.Instance.LogError("Block colliding with wrong objects: " + other.gameObject.name, gameObject);
                }
            }
        }

        private void OnDestroy()
        {
            snake.onEnterIframes -= OnIframeEnter;
            snake.onExitIframes -= OnIframeExit;
        }

        #endregion

        #region Public Functions

        public void ToggleCover(bool enabled)
        {
            cover.SetActive(enabled);
        }

        public void RegisterLastPositionAndRotation(Vector3 pos, Quaternion rot)
        {
            movementRecords.Push(new MovementRecord(pos, rot));
        }

        public virtual void SetSnake(GameObject s)
        {
            SnakeMovementBase _snake = s.GetComponent<SnakeMovementBase>();

            if (_snake != null)
            {
                this.snake = _snake;
                _collider.isTrigger = true;
                snake.onEnterIframes += OnIframeEnter;
                snake.onExitIframes += OnIframeExit;
            }
        }               

        public void RecordTravelPoint()
        {
            _lastTimeTravelPos = movementRecords.Peek().regPos;
            _lastTimeTravelRot = movementRecords.Peek().regRot;
        }

        public void GoBackInTime()
        {
            if(_lastTimeTravelPos.x == -100) //this block did not exist at the time
            {                
                GameManager.Instance.RemoveBlock(snake.bodyParts, this);
            }
            else
            {
                DebugHelper.Instance.Log(snake.gameObject.name + "block: " + gameObject.name + " old pos: " + transform.position, gameObject);

                _collider.enabled = false;               

                transform.DOMove(_lastTimeTravelPos, 0.6f).SetUpdate(true).OnComplete(() => 
                {
                    transform.rotation = _lastTimeTravelRot;
                    _collider.enabled = true;
                });
            }
        }

        public void TintBlock()
        {
            Material[] temp = meshRenderer.materials;

            for (int i = 0; i < temp.Length; i++)
            {
                if (i == materialChangeIndex)
                {
                    temp[i] = GameManager.Instance.colorScheme.GetMaterialByIndex(snake.GetNumber() - 1);
                }
            }

            meshRenderer.materials = temp;
        }

        public void TintBlockBG()
        {
            Material[] temp = meshRendererBG.materials;

            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = GameManager.Instance.colorScheme.GetMaterialByIndex(snake.GetNumber() - 1);
            }

            meshRendererBG.materials = temp;
        }

        public void TintBlock(Material materialToTint)
        {
            Material[] temp = meshRendererBG.materials;

            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = materialToTint;
            }

            meshRendererBG.materials = temp;
        }

        public virtual void ActivatePassiveEffect() { }

        public void OnIframeEnter()
        {          
            StartCoroutine(IframeCo());
        }

        public void OnIframeExit()
        {
            if (meshRenderer == null) return;

            meshRenderer.gameObject.SetActive(true);
        }

        public bool IsHead()
        {
            return _head;
        }

        public void SetHead(bool isHead)
        {
            _head = isHead;
        }

        public void PunchScale()
        {
            if (!_inAnimation)
            {
                _inAnimation = true;

                meshRenderer.transform.DOPunchScale(Vector3.one * EffectsHelper.Instance.p_scale, EffectsHelper.Instance.p_duration, 5, 1).OnComplete(() => 
                {
                    _inAnimation = false;
                });
            }
        }

        public void GrowScale(bool playSound = false)
        {
            if (!_inAnimation)
            {
                _inAnimation = true;

                transform.DOScale(1f, EffectsHelper.Instance.growDuration).From(0).OnComplete(() => 
                {
                    _inAnimation = false;

                    if (playSound)
                    {
                        AudioController.Instance.PlayAudio(Audio.AudioType.SFX_SPAWN);
                    }
                });
            }
        }

        public bool IsFreeBlock() //checks if the block was already picked by some snake
        {
            return !cover.activeInHierarchy;
        }

        public void DestroyWithTheSnake() //called before destroying a player's snake
        {
            if (IsFreeBlock())
            {
                Destroy(gameObject);
            }
        }

        #endregion

        #region Private Functions

        protected void ActivatePower(GameObject other) { }

        IEnumerator IframeCo()
        {
            while (snake.inIframes)
            {
                meshRenderer.gameObject.SetActive(false);

                yield return new WaitForSeconds(GameManager.Instance.flashDuration);

                meshRenderer.gameObject.SetActive(true);

                yield return new WaitForSeconds(GameManager.Instance.flashDuration);
            }
        }
        #endregion
    }
}
