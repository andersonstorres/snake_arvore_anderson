﻿using Snake;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

namespace Block
{
    public class EnginePower : BlockBase
    {
        [SerializeField] public float speedToAdd = 0.2f;

        public float SpeedToAdd { get => speedToAdd; private set => speedToAdd = value; }

        #region Unity Functions
        #endregion

        #region Public Functions

        public override void ActivatePassiveEffect()
        {
            base.ActivatePassiveEffect();
            snake.GetComponent<SnakeSetupBase>().PowerToEngine(SpeedToAdd);
            snake.GetComponent<SnakeSetupBase>().AddToCountByType(BlockType.ENGINE_POWER);
        }

        #endregion

        #region Private Functions
        #endregion
    }
}