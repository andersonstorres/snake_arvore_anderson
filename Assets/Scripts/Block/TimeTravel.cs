﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using Snake;

namespace Block
{
    public class TimeTravel : BlockBase
    {
        #region Unity Functions
        #endregion

        #region Public Functions

        public override void ActivatePassiveEffect()
        {
            base.ActivatePassiveEffect();
            snake.GetComponent<SnakeSetupBase>().AddToCountByType(BlockType.TIME_TRAVEL);
            GameManager.Instance.SetTimeTravelPoint();
        }

        #endregion

        #region Private Functions
        #endregion
    }
}
