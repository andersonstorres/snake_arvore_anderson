﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Block;
using Snake;
using System;
using Random = UnityEngine.Random;

namespace Core
{
    public class BlocksSpawner : MonoBehaviour
    {
        public static BlocksSpawner Instance;

        public Transform topLeft;
        public Transform bottomRight;
        [SerializeField] GameObject[] availableBlocks;

        public event Action<BlockBase> onSpawnBlock;

        GameObject _blockHolder;

        #region Unity Functions

        private void Awake()
        {
            Instance = this;
        }

        #endregion

        #region Public Functions

        public GameObject GetRandomBlockPrefab()
        {
            return availableBlocks[Random.Range(0, availableBlocks.Length)];
        }

        public GameObject GetBlockPrefabByType(BlockType blockType)
        {
            GameObject block = null;

            for (int i = 0; i < availableBlocks.Length; i++)
            {
                if (availableBlocks[i].GetComponent<BlockBase>().type == blockType)
                {
                    block = availableBlocks[i];
                    break;
                }
            }

            if (block == null)
            {
                DebugHelper.Instance.LogError("Block prefab not found: " + blockType, gameObject);
            }

            return block;
        }

        public void AskForNewBlock(GameObject snake)
        {
            if (CanSpawn())
            {
                SnakeMovementBase player = snake.GetComponent<SnakeMovementBase>();

                if (player != null)
                {
                    SpawnBlock(player, 2);
                }
            }
        }

        public Vector3 GetRandomGridPosition(float offset = 0)
        {
            int horizontal = Mathf.RoundToInt(Random.Range(topLeft.position.x + offset, bottomRight.position.x - offset));
            int vertical = Mathf.RoundToInt(Random.Range(bottomRight.position.z + offset, topLeft.position.z - offset));

            Vector3 pos = new Vector3(horizontal, 0, vertical);

            //0 is not considered a valid position to spawn something
            if(pos == Vector3.zero)
            {
                pos += Vector3.forward;
            }

            return pos;
        }

        #endregion

        #region Private Functions

        void SpawnBlock(SnakeMovementBase snake, float offset = 0)
        {
            //create at runtime just for readability
            if (_blockHolder == null)
            {
                _blockHolder = new GameObject("blocks_holder");
            }

            Vector3 spawnPos = GetRandomGridPosition(offset);

            SpawnVFX(spawnPos);

            GameObject block = Instantiate(GetRandomBlockPrefab(), spawnPos, Quaternion.identity); //instantiate at position with no rotation
            block.transform.SetParent(_blockHolder.transform);
            block.gameObject.name += " - snake " + snake.GetNumber(); //for readability

            //cover (thev inverse block mesh) it's used to check if a block has already been collected by a player or not
            block.GetComponent<BlockBase>().ToggleCover(false);

            //every blocks has a target snake, the AI compare this 
            //with your player snake information to decide to catch or not
            block.GetComponent<BlockBase>().snake = snake;
            //currently used to destroy blocks after a player dies, could be used in future for other stuff
            block.GetComponent<BlockBase>().originalSnake = snake.GetComponent<SnakeMovementPlayer>();

            //customize the block with the color of the designated playe
            block.GetComponent<BlockBase>().TintBlock();

            block.GetComponent<BlockBase>().GrowScale(true);

            //AI need to know the current block to be collected before calculate the path
            onSpawnBlock?.Invoke(block.GetComponent<BlockBase>());
        }

        private static void SpawnVFX(Vector3 spawnPos)
        {
            GameObject effect = PoolingManager.Instance.UseObject(EffectsHelper.Instance.spawnBlockEffects, spawnPos + (Vector3.up * 0.5f), Quaternion.identity);

            PoolingManager.Instance.ReturnObject(effect, 1.5f);
        }

        bool CanSpawn()
        {
            return true;
        }

        #endregion
    }
}