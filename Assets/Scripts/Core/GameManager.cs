﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Snake;
using Block;
using Score;
using Audio;
using Utils;
using UI;
using UnityEngine.SceneManagement;

public enum SceneRef
{
    MAIN_MENU, GAMEPLAY
}

public enum GameplayState
{
    INGAME, GAMEOVER, PAUSE
}

public enum Difficulty
{
    EASY, NORMAL, HARD
}

namespace Core
{
    //Important: functions here cannot be linked through the inspector to the game object because this object will navigate between scenes
    public class GameManager : MonoBehaviour
    {
        #region Singleton
        private static GameManager _instance;

        public static GameManager Instance { get { return _instance; } }

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;

                if (loadOptions)
                    LoadOptionsPrefs();
            }

            DontDestroyOnLoad(this.gameObject);
        }
        #endregion

        [SerializeField] bool loadOptions = true;

        public SceneRef currentScene; //update when load new scene, should have the same index as the build config
        public GameplayState currentGameplayState; //used only in the game scene, tests for ingame or other state
        public ColorScheme colorScheme; //helper with the snakes colors
        public List<PlayerSetup> playerSetups = new List<PlayerSetup>(); //players infos that need to be instantiated
        public List<GameObject> players = new List<GameObject>(); //contains the snakes in the current match

        [SerializeField] GameObject snakePrefab; //player snake
        [SerializeField] GameObject snakeAIPrefab; //ai snake

        //SETTINGS
        [Header("Iframes")]
        [SerializeField] bool useIframes = false;
        public float flashDuration = 0.15f;
        [SerializeField] float iframesSeconds = 3;

        [Header("AI")]
        [SerializeField] Difficulty _difficulty = Difficulty.NORMAL;
        [SerializeField] bool _aiUsePowers = true;
        [SerializeField] int _aiSkipTurns = 3;
        [SerializeField] float _recalculatePercentChance = 70;

        bool _traveling; //flag to verify if a time traveling is happening
        GameObject _snakeHolder; //created at runtime, hold the snakes objects for readability

        public bool Traveling { get => _traveling; private set => _traveling = value; }

        [System.Serializable]
        public class PlayerSetup //responsible for carrying the information between MainMenu and Gameplay
        {
            public int number;
            public SnakeTypes initialSnake;
            public KeyCode leftKey;
            public KeyCode rightKey;

            public PlayerSetup(int number, SnakeTypes initialSnake, KeyCode leftKey, KeyCode rightKey)
            {
                this.number = number;
                this.initialSnake = initialSnake;
                this.leftKey = leftKey;
                this.rightKey = rightKey;
            }
        }

        #region Unity Functions

        private void OnEnable()
        {
            SceneManager.sceneLoaded += LevelLoaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= LevelLoaded;
        }

        private void Start()
        {
            if(playerSetups.Count == 0 && currentScene == SceneRef.GAMEPLAY)
            {
                LoadScene(SceneRef.MAIN_MENU);
            }

            //play the main soundtrack
            AudioController.Instance.PlayAudio(Audio.AudioType.ST_01, true, true, 0);
        }

        private void OnApplicationQuit()
        {
            SaveOptionsPrefs();
        }

        #endregion

        #region Public Functions

        public void Quit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
         Application.OpenURL(webplayerQuitURL);
#else
         Application.Quit();
#endif
        }

        #region Setup

        //center the loads here
        public void LoadScene(SceneRef sceneToLoad)
        {
            //here could have some validation

            SceneChanger.Instance.LoadScene(sceneToLoad);
        }

        public void StartGame() //assign to the button on MainMenu
        {
            LoadScene(SceneRef.GAMEPLAY);
        }

        public string FormatKey(KeyCode keyToConvert)
        {
            string s1 = keyToConvert.ToString();
            string s2 = s1.Substring(s1.Length - 1);

            return s2;
        }

        //update a snake setup on MainMenu
        public void AddOrUpdatePlayer(int number, SnakeTypes initialSnakeType, KeyCode leftKey, KeyCode rightKey)
        {
            for (int i = 0; i < playerSetups.Count; i++)
            {
                if(playerSetups[i].number == number)
                {
                    //Update
                    playerSetups[i].initialSnake = initialSnakeType;
                    playerSetups[i].leftKey = leftKey;
                    playerSetups[i].rightKey = rightKey;

                    return;
                }
            }

            //Add
            playerSetups.Add(new PlayerSetup(number, initialSnakeType, leftKey, rightKey));
        }

        #endregion

        #region Gameplay  

        #region Time Travel

        //records the position and rotation of all blocks inside snakes
        public void SetTimeTravelPoint()
        {
            if (Traveling) return;

            BlockBase[] bb = FindObjectsOfType<BlockBase>();

            for (int i = 0; i < bb.Length; i++)
            {
                if (!bb[i].IsFreeBlock())
                {
                    bb[i].RecordTravelPoint();
                }
            }
        }

        //returns all blocks to the position and rotation of the 
        //last record of SetTimeTravelPoint() and destroys those that were acquired later
        public void TimeTravel()
        {
            if (Traveling) return; //don't time travel if already time traveling

            AudioController.Instance.PlayAudio(Audio.AudioType.SP_REWIND);

            StartCoroutine(TimeTravelCo()); //set the flag false after 1 second

            BlockBase[] bb = FindObjectsOfType<BlockBase>();

            for (int i = 0; i < bb.Length; i++)
            {
                //only blocks inside snakes go back in time
                if (!bb[i].IsFreeBlock())
                {
                    bb[i].GoBackInTime();
                }
            }
        }

        #endregion

        #region Blocks & Snake

        //the main method for remove blocks inside snakes, 
        //delay it's used to prevent conflicts the my occur between different collisons and powers
        public void RemoveBlock(List<Transform> listToRemove, BlockBase block, SnakeScore snakeScore = null, float delay = 0.2f)
        {
            StartCoroutine(RemoveCo(listToRemove, block, snakeScore, delay));
        }

        IEnumerator RemoveCo(List<Transform> listToRemove, BlockBase block, SnakeScore snakeScore = null, float delay = 0.2f)
        {
            yield return new WaitForSeconds(delay); //remove just after a variable delay to prevent conflicts

            if (block == null) //check if after delay the block still exists
                yield break;

            if (snakeScore != null) //check if the block is a player block and update the type count at top of the screen
            {
                snakeScore.RemoveBlock(block.type); //score tracker
            }

            block.snake.GetComponent<SnakeSetupBase>().RemoveFromCountByType(block.type); //add and remove tracker

            if (block.type == BlockType.ENGINE_POWER)
            {
                //engine power need to keep a count of engine power too
                block.snake.GetComponent<SnakeSetupBase>().RemovePowerFromEngine(block.GetComponent<EnginePower>().SpeedToAdd);
            }

            ExplosionVFX(block);


            //remove from the list and destroy at the end
            SnakeMovementBase tempSnake = block.snake;

            //remove from list
            int index = listToRemove.IndexOf(block.transform);
            Transform t = listToRemove[index];
            listToRemove.RemoveAt(index);

            //destroy
            Destroy(t.gameObject);

            tempSnake.CheckDeath(); //prevent 0 blocks snake
        }


        //the main method for remove snakes
        public void RemoveSnake(GameObject snakeToRemove, bool player)
        {
            //probably already removed
            if(snakeToRemove == null)
            {
                return;
            }

            //remove from the other list
            if (player)
            {
                players.Remove(snakeToRemove);
                SnakeMovementPlayer tempP = snakeToRemove.GetComponent<SnakeMovementPlayer>();
                tempP.CallForDestruction(); //trigger the event that will destroy loose blocks  
                EliminatedMsg.Instance.ShowMessage(tempP.GetNumber(), 1.2f);
            }

            //finally destroy
            Destroy(snakeToRemove);

            //check if still has players
            if(players.Count == 0)
            {
                PauseHandler.Instance.EnterGameOver();
                currentGameplayState = GameplayState.GAMEOVER;                
            }
        }

        //players ask for an AI at the beginning of the move, 
        //then when an AI dies, before it is erased, it asks for another
        public GameObject AskForAI(GameObject snake)        
        {
            SnakeSetupPlayer player = snake.GetComponent<SnakeSetupPlayer>();

            //every AI should be connect to a player, having the same lifespan or less
            if (player != null)
            {
                GameObject ai = Instantiate(snakeAIPrefab, _snakeHolder.transform);
                ai.gameObject.name = "snake AI " + player.number;

                ai.GetComponent<SnakeSetupAI>().SetupAI(player);

                return ai;
            }

            //player was destroyed in the meantime
            DebugHelper.Instance.LogWarning("No setup found", gameObject);
            return null;
        }

        #endregion

        #region Options

        public void SaveOptionsPrefs()
        {
            PlayerPrefs.SetInt("useIframes", GetUseIframes() ? 1 : 0);
            PlayerPrefs.SetInt("aiPowers", GetAIUsePowers() ? 1 : 0);
            PlayerPrefs.SetInt("difficulty", (int)GetDifficulty());
        }

        //when start to move or after a hit the snake gain a certain time in seconds of iframes
        public bool GetUseIframes()
        {
            return useIframes;
        }

        public void SetUseIframes(bool enable)
        {
            useIframes = enable;
        }

        public float GetIframesSeconds()
        {
            float sec = iframesSeconds;

            switch (_difficulty)
            {
                case Difficulty.EASY:

                    sec = iframesSeconds * 2f;

                    break;
                case Difficulty.NORMAL:

                    sec = iframesSeconds;

                    break;
                case Difficulty.HARD:

                    sec = iframesSeconds * 0.75f;

                    break;
            }

            return sec;
        }

        //blocks inside an AI may or may not be activated
        public bool GetAIUsePowers()
        {
            return _aiUsePowers;
        }

        public void SetAIUsePowers(bool enable)
        {
            _aiUsePowers = enable;
        }

        //difficulty affect the chance and speed to recalculate path that AI will have
        public Difficulty GetDifficulty()
        {
            return _difficulty;
        }

        public void SetDifficulty(Difficulty difficulty)
        {
            this._difficulty = difficulty;
        }

        public int GetTurnsToSkip()
        {
            int turns = _aiSkipTurns;

            switch (_difficulty)
            {
                case Difficulty.EASY:

                    turns = _aiSkipTurns * 2;

                    break;
                case Difficulty.NORMAL:

                    turns = _aiSkipTurns;

                    break;
                case Difficulty.HARD:

                    turns = Mathf.CeilToInt((float)_aiSkipTurns / 2f);

                    break;
            }

            return turns;
        }

        public float GetRecalculatePercentage()
        {
            float percent = _recalculatePercentChance;

            switch (_difficulty)
            {
                case Difficulty.EASY:

                    percent = _recalculatePercentChance / 2f;

                    break;
                case Difficulty.NORMAL:

                    percent = _recalculatePercentChance;

                    break;
                case Difficulty.HARD:

                    percent = 100f;

                    break;
            }

            return percent;
        }

        #endregion

        #endregion

        #endregion

        #region Private Functions

        //take some actions based on the last scene loaded, 
        //listener of SceneManager.sceneLoaded
        private void LevelLoaded(Scene scene, LoadSceneMode mode)
        {
            if (GameManager.Instance != this) return;

            currentScene = (SceneRef)scene.buildIndex;

            switch (currentScene)
            {
                case SceneRef.MAIN_MENU:

                    playerSetups.Clear();

                    break;
                case SceneRef.GAMEPLAY:

                    if (playerSetups.Count > 0)
                    {
                        currentGameplayState = GameplayState.INGAME;
                        InstantiatePlayers();
                    }

                    break;
            }
        }

        //add players on the Gameplay scene and passes the setup
        void InstantiatePlayers()
        {
            if (_snakeHolder == null)
            {
                //create at runtime for readability
                _snakeHolder = new GameObject("snakes_holder");
            }

            foreach(PlayerSetup ps in playerSetups)
            {
                GameObject player = Instantiate(snakePrefab, _snakeHolder.transform);
                player.gameObject.name = "snake " + ps.number;
                players.Add(player);

                player.GetComponent<SnakeSetupPlayer>().SetupPlayer(ps);

                ScoreManager.Instance.CreateScore(player.GetComponent<SnakeScore>());
            }
        }

        private static void ExplosionVFX(BlockBase block)
        {
            AudioController.Instance.PlayAudio(Audio.AudioType.SFX_EXPLOSION);

            GameObject explosion = PoolingManager.Instance.UseObject(EffectsHelper.Instance.electricExplosion, block.transform.position + Vector3.up, Quaternion.identity);
            PoolingManager.Instance.ReturnObject(explosion, 1.5f);
        }

        IEnumerator TimeTravelCo()
        {
            //handles the flag and screen effects

            Traveling = true;
            EffectsHelper.Instance.rewindEffect.enabled = true;
            EffectsHelper.Instance.rewindUI.SetActive(true);

            yield return new WaitForSecondsRealtime(1f);

            EffectsHelper.Instance.rewindUI.SetActive(false);
            EffectsHelper.Instance.rewindEffect.enabled = false;
            Traveling = false;
        }

        void LoadOptionsPrefs()
        {
            SetUseIframes((PlayerPrefs.GetInt("useIframes", 1) == 1) ? true : false);
            SetAIUsePowers((PlayerPrefs.GetInt("aiPowers", 1) == 1) ? true : false);
            SetDifficulty((Difficulty)(PlayerPrefs.GetInt("difficulty", (int)Difficulty.NORMAL)));
        }

        #endregion
    }
}
