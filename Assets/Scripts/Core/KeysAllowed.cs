﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    [CreateAssetMenu(fileName = "Keys Allowed Data", menuName = "New Keys Allowed Data")]
    public class KeysAllowed : ScriptableObject
    {
        public List<KeyCode> keys = new List<KeyCode>();

        public bool CheckIfAllowed(KeyCode key)
        {
            return keys.Contains(key);
        }  
    }
}
