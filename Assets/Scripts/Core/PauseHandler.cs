﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Audio;
using Score;
using UI;

namespace Core
{
    public class PauseHandler : MonoBehaviour
    {
        public static PauseHandler Instance;

        [SerializeField] GameObject pauseScreen;
        [SerializeField] GameObject gameOverScreen;

        #region Unity Functions

        private void Awake()
        {
            Instance = this;
        }

        private void Update()
        {
            switch (GameManager.Instance.currentGameplayState)
            {
                case GameplayState.INGAME:

                    if (Input.GetKeyDown(KeyCode.Escape) && CanPause())
                    {
                        AudioController.Instance.PlayAudio(Audio.AudioType.UI_BUTTON_CLICK);
                        EnterPause();
                    }

                    break;
                case GameplayState.GAMEOVER:

                    if (gameOverScreen.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))
                    {
                        Time.timeScale = 1f;
                        gameOverScreen.SetActive(false);
                        GameManager.Instance.LoadScene(SceneRef.MAIN_MENU);
                    }

                    break;
                case GameplayState.PAUSE:

                    if(pauseScreen.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))
                    {
                        AudioController.Instance.PlayAudio(Audio.AudioType.UI_BUTTON_CLICK);
                        Time.timeScale = 1f;
                        pauseScreen.SetActive(false);
                        GameManager.Instance.currentGameplayState = GameplayState.INGAME; //maybe move this line to GameManager
                    }

                    break;
            }
        }

        #endregion

        #region Public Functions
        public void EnterGameOver()
        {
            AudioController.Instance.PlayAudio(Audio.AudioType.SP_END);
            
            Time.timeScale = 0;

            gameOverScreen.SetActive(true);

            EliminatedMsg.Instance.ForceHide();

            ScoreManager.Instance.CreateFinalScore();
        }

        #endregion

        #region Private Functions

        void EnterPause()
        {
            Time.timeScale = 0;
            pauseScreen.SetActive(true);
            GameManager.Instance.currentGameplayState = GameplayState.PAUSE;
        }

        bool CanPause()
        {
            if (GameManager.Instance.Traveling)
            {
                return false;
            }

            return true;
        }

        public void ReturnToMainMenu()
        {
            if(GameManager.Instance.currentGameplayState == GameplayState.PAUSE)
            {
                AudioController.Instance.PlayAudio(Audio.AudioType.UI_BUTTON_CLICK);
                Time.timeScale = 1f;
                pauseScreen.SetActive(false);

                GameManager.Instance.LoadScene(SceneRef.MAIN_MENU);
            }
        }

        #endregion
    }
}
