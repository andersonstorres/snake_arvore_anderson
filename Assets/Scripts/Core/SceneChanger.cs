﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UI;
using DG.Tweening;

namespace Core
{
    public class SceneChanger : MonoBehaviour
    {
        #region Singleton
        private static SceneChanger _instance;

        public static SceneChanger Instance { get { return _instance; } }

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }

            DontDestroyOnLoad(this.gameObject);
        }
        #endregion

        [SerializeField] CanvasGroup loadPanel;
        [SerializeField] TextMeshProUGUI loadPercent;
        [SerializeField] float fadeInTime = 1f;
        [SerializeField] float fadeOutTime = 0.4f;

        #region Unity Functions
        #endregion

        #region Public Functions

        public void LoadScene(SceneRef scene)
        {
            switch (GameManager.Instance.currentScene)
            {
                case SceneRef.MAIN_MENU:

                    FindObjectOfType<MainMenuUI>().HideAllPanels();

                    break;
                case SceneRef.GAMEPLAY:
                    break;
            }

            loadPanel.DOFade(1, fadeInTime).SetUpdate(true).OnComplete(() => 
            {
                StartCoroutine(LoadYourAsyncScene((int)scene));
            });            
        }

        #endregion

        #region PrivateFunctions

        IEnumerator LoadYourAsyncScene(int sceneIndex)
        {
            yield return null;

            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneIndex);
            asyncOperation.allowSceneActivation = false;

            while (!asyncOperation.isDone)
            {
                loadPercent.text = "Loading  " + ((asyncOperation.progress + 0.1f) * 100).ToString("F0") + "%";

                if (asyncOperation.progress >= 0.9f)
                {
                    asyncOperation.allowSceneActivation = true;
                }

                yield return null;
            }

            Time.timeScale = 0;

            loadPanel.DOFade(0, fadeOutTime).SetUpdate(true).OnComplete(() => 
            {
                Time.timeScale = 1;
            });          

            
        }

        #endregion
    }
}
