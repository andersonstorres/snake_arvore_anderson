﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Score.UI;
using Snake;
using System.Linq;
using UI;

namespace Score
{
    public class ScoreManager : MonoBehaviour
    {
        public static ScoreManager Instance;

        [SerializeField] GameObject scorePrefab;
        [SerializeField] GameObject scoreboardPrefab;
        [SerializeField] Transform scoreHolder;
        [SerializeField] Transform finalScoreHolder;
 
        [SerializeField] List<SnakeAndScore> scores = new List<SnakeAndScore>(); 


        [System.Serializable]
        public class SnakeAndScore
        {
            public SnakeScore snakeScore;
            public ScoreUI scoreUI;

            public SnakeAndScore(SnakeScore snakeScore, ScoreUI scoreUI)
            {
                this.snakeScore = snakeScore;
                this.scoreUI = scoreUI;
            }
        }

        #region Unity Functions

        private void OnEnable()
        {
            foreach (Transform child in scoreHolder)
            {
                Destroy(child.gameObject);
            }

            foreach (Transform child in finalScoreHolder)
            {
                Destroy(child.gameObject);
            }
        }

        private void Awake()
        {
            Instance = this;
        }

        #endregion

        #region Public Functions

        //keep track of the data (SnakeScore) and the UI (ScoreUI)
        public void CreateScore(SnakeScore snake)
        {
            GameObject newScore = Instantiate(scorePrefab, scoreHolder);
            ScoreUI newScoreUI = newScore.GetComponent<ScoreUI>();

            newScoreUI.Setup(snake);

            scores.Add(new SnakeAndScore(snake, newScoreUI));
        }

        public void UpdateScore(int index)
        {
            //if the snake score has not been registred, just return. Not supposed to happen
            if (scores[index].snakeScore == null)
            {
                return;
            }

            //updates UI separatelly
            scores[index].scoreUI.UpdateScore(scores[index].snakeScore);

            //keep track of the best player score (could be reseted in PlayerSetupUI
            if ((PlayerPrefs.GetInt("bestScore" + index, 0)) < (scores[index].snakeScore.Score))
            {
                PlayerPrefs.SetInt("bestScore" + index, scores[index].snakeScore.Score);
            }

            //keeps track of the last score achieved to show on the game over / end of match screen
            PlayerPrefs.SetInt("lastScore" + index, scores[index].snakeScore.Score);
        }

        //creates another list in order of current score and instantiate ScoreBoardItem prefabs to show
        public void CreateFinalScore()
        {
            List<SnakeAndScore> orderedList = new List<SnakeAndScore>();
            orderedList = scores.OrderBy(go => go.snakeScore.Score).ToList();
            orderedList.Reverse();

            for (int i = 0; i < orderedList.Count; i++)
            {
                GameObject tempPanel = Instantiate(scoreboardPrefab, finalScoreHolder);

                bool winner = false;

                if(i == 0 || orderedList[i].snakeScore.Score == orderedList[0].snakeScore.Score)
                {
                    winner = true;
                }

                tempPanel.GetComponent<ScoreboardItem>().Setup(orderedList[i].snakeScore, winner);
            }
        }

        #endregion

        #region Private Functions
        #endregion
    }
}
