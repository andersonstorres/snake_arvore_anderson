﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Core;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Snake;
using UI;

namespace Score.UI
{
    public class ScoreUI : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI playerNumber;
        [SerializeField] TextMeshProUGUI scorePoints;
        [SerializeField] TextMeshProUGUI engineTextQty, timeTravelTextQty, batteringTextQty, leftKeyText, rightKeyText;

        PulseUI pulseUI;

        #region Unity Functions

        private void Awake()
        {
            pulseUI = GetComponent<PulseUI>();
        }

        #endregion

        #region Public Functions

        public void Setup(SnakeScore snakeScore)
        {
            GetComponent<UIGridRenderer>().color = GameManager.Instance.colorScheme.GetColorByIndex(snakeScore.GetNumber() - 1); ;

            //change texts color
            foreach (TextMeshProUGUI txt in GetComponentsInChildren<TextMeshProUGUI>())
            {
                txt.color = GameManager.Instance.colorScheme.GetColorByIndex(snakeScore.GetNumber() - 1);
            }

            //change icons colors
            foreach(Image img in GetComponentsInChildren<Image>())
            {
                img.color = GameManager.Instance.colorScheme.GetColorByIndex(snakeScore.GetNumber() - 1); 
            }

            leftKeyText.text = GameManager.Instance.FormatKey(snakeScore.GetLeftKey());
            rightKeyText.text = GameManager.Instance.FormatKey(snakeScore.GetRightKey());
        }

        public void ResetScore()
        {
            scorePoints.text = "0000";
            engineTextQty.text = "0";
            timeTravelTextQty.text = "0";
            batteringTextQty.text = "0";
        }

        public void UpdateScore(SnakeScore snakeScore)
        {
            scorePoints.text = snakeScore.Score.ToString("0000");
            engineTextQty.text = Mathf.Max(snakeScore.EngineQty, 0).ToString();
            timeTravelTextQty.text = Mathf.Max(snakeScore.TimeTravelQty, 0).ToString();
            batteringTextQty.text = Mathf.Max(snakeScore.BatteringRamQty, 0).ToString();

            if (pulseUI != null)
            {
                pulseUI.SinglePulse();
            }
        }

        #endregion

        #region Private Functions
        #endregion
    }
}
