﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Snake;
using Core;
using UnityEngine.UI.Extensions;

namespace Score.UI
{
    public class ScoreboardItem : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI playerName;
        [SerializeField] TextMeshProUGUI lastScore;
        [SerializeField] TextMeshProUGUI bestScore;
        [SerializeField] GameObject winnerIcon;

        UIGridRenderer uIGridRenderer;

        #region Unity Functions

        private void Awake()
        {
            uIGridRenderer = GetComponent<UIGridRenderer>();
        }

        #endregion

        #region Public Functions

        public void Setup(SnakeScore snakeScore, bool winner)
        {
            Color customColor = GameManager.Instance.colorScheme.GetColorByIndex(snakeScore.GetNumber() - 1);

            uIGridRenderer.color = customColor;
            playerName.color = customColor;
            lastScore.color = customColor;
            bestScore.color = customColor;

            winnerIcon.SetActive(winner);

            int index = snakeScore.GetNumber() - 1;

            playerName.text = "P" + snakeScore.GetNumber();
            lastScore.text = (PlayerPrefs.GetInt("lastScore" + index, 0)).ToString("0000");
            bestScore.text = (PlayerPrefs.GetInt("bestScore" + index, 0)).ToString("0000");
        }

        #endregion

        #region Private Functions
        #endregion
    }
}