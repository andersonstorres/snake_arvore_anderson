﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Block;
using Core;
using System.Linq;
using AI;
using Score;

namespace Snake
{
    public class SnakeMovementAI : SnakeMovementBase
    {
        [SerializeField] Vector3 targetPos;

        SnakeSetupAI _snakeSetupAI;
        AIController _aIController;

        int directionCorrector = 1;
        bool recalculateFlag;
        int turnsToSkip = 0;

        #region Unity Functions

        private void Awake()
        {
            _snakeSetup = GetComponent<SnakeSetupAI>();
            _snakeSetupAI = _snakeSetup.GetComponent<SnakeSetupAI>();

            _aIController = GetComponent<AIController>();
        }

        private void Update()
        {
            if (inIframes)
            {
                if (_iframesLeft > 0)
                {
                    _iframesLeft -= Time.deltaTime;
                }
                else
                {
                    ExitIframes();
                }
            }
        }

        private void OnDestroy()
        {
            BlocksSpawner.Instance.onSpawnBlock -= SetTargetPosition;
        }

        private void OnDisable()
        {
            BlocksSpawner.Instance.onSpawnBlock -= SetTargetPosition;
        }

        #endregion

        #region Public Functions

        public override void StartMovement()
        {
            EnterIframes();

            _moveCoroutine = StartCoroutine(MoveCo());           
        }

        public override void HandleCollision(BlockBase head, BlockBase other)
        {
            if (inIframes)
            {
                return;
            }

            if (!GameManager.Instance.GetAIUsePowers())
            {
                Die();
                return;
            }

            DebugHelper.Instance.Log(gameObject.name + " block: " + head + " HIT " + other.snake + " block " + other.type, gameObject);
            HitVFX(head);

            #region Time Travel
            BlockBase tempBlock = FindBlockByType(BlockType.TIME_TRAVEL);

            //found a block?
            if (tempBlock != null)
            {
                //he really has the correct script
                TimeTravel tempTime = tempBlock.GetComponent<TimeTravel>();

                if (tempTime != null)
                {
                    EnterIframes();

                    //remove the own block
                    GameManager.Instance.RemoveBlock(bodyParts, tempTime, null, 0);

                    //travel in time
                    GameManager.Instance.TimeTravel();

                    recalculateFlag = true;

                    return;
                }
            }
            #endregion

            #region Battering Ram
            tempBlock = FindBlockByType(BlockType.BATTERING_RAM, true);

            //found a block?
            if (tempBlock != null)
            {
                //he really has the correct script?
                BatteringRam tempBat = tempBlock.GetComponent<BatteringRam>();

                if (tempBat != null)
                {
                    //there's snake enough to handle a block destruction?
                    if (bodyParts.Count > 1)
                    {
                        //hit other snake?
                        if (other.snake != head.snake)
                        {
                            //the other block is the head?
                            if (other.IsHead())
                            {
                                //it's not a battering or time travel (priority)
                                if (other.type != BlockType.BATTERING_RAM && other.type != BlockType.TIME_TRAVEL)
                                {
                                    //remove the other block
                                    GameManager.Instance.RemoveBlock(other.snake.bodyParts, other, other.snake.GetComponent<SnakeScore>(), 0.35f);
                                }
                            }
                            else
                            {
                                //remove the other block
                                GameManager.Instance.RemoveBlock(other.snake.bodyParts, other, other.snake.GetComponent<SnakeScore>(), 0.35f);
                            }
                        }
                        //hit iself?
                        else
                        {
                            //is the block that will already be destroyed?
                            if (other != tempBat)
                            {
                                //remove your other block
                                GameManager.Instance.RemoveBlock(other.snake.bodyParts, other, null, 0.28f);
                            }
                        }

                        //remove the own block
                        GameManager.Instance.RemoveBlock(bodyParts, tempBat, null, 0.2f);

                        EnterIframes();

                        return;
                    }
                }
            }
            #endregion

            Die();
        }

        public override void Die()
        {
            base.Die();

            GameObject nemesis = GetNemesis();

            if (nemesis != null && !nemesis.GetComponent<SnakeMovementBase>().dead)
            {
                //call for a new ai
                GameObject ai = GameManager.Instance.AskForAI(GetNemesis());
                //transfer the knowledge and assign a new nemesis
                ai.GetComponent<SnakeMovementAI>().targetPos = targetPos;
                GetNemesis().GetComponent<SnakeSetupPlayer>().SetNemesis(ai);
            }            

            //finally ask to be removed            
            GameManager.Instance.RemoveSnake(this.gameObject, false);
        }

        public void SetTargetPosition(BlockBase block)
        {
            //the target block is used by the pathfinder to calculate the path,
            //positio is used because the block could be destroyed
           
            if (block.snake.gameObject == GetNemesis())
            {
                targetPos = block.transform.position;
                turnsToSkip = GameManager.Instance.GetTurnsToSkip();
            }
        }

        public void DecideToRecalculate()
        {
            //the AI may or may not recalculate the path after the player 
            //collect the target block, chances of this happens is affected 
            //by the difficulty settings

            float chance = Random.value;

            if (chance < (GameManager.Instance.GetRecalculatePercentage() / 100f))
            {
                recalculateFlag = true;
            }
            else
            {
                recalculateFlag = false;
            }
        }

        #endregion

        #region Private Functions

        IEnumerator MoveCo()
        {
            while (true && !Dead)
            {
                //still has a body?
                if (bodyParts.Count <= 0) break;

                //any time travel occuring?
                if (GameManager.Instance.Traveling)
                {
                    recalculateFlag = true;
                    yield return null;
                    continue;
                }

                //extra, sometimes the AI could get confused
                if ((bodyParts[0] == null || !bodyParts[0].GetComponent<BlockBase>().IsHead()) && bodyParts.Count > 0)
                {
                    UpdateHead();
                }

                //caches the current transform state of the the fist block
                _previousPosition = bodyParts[0].position;
                _previousRotation = bodyParts[0].rotation;

                //keeps track of all the blocks movements, currently used to get info for the time travel,
                //but in the future could be used for other stuff like an mirror snake
                bodyParts[0].GetComponent<BlockBase>().RegisterLastPositionAndRotation(_previousPosition, _previousRotation);

                //AI skip some turns of movement before recalcute the path
                if (turnsToSkip == 0)
                {
                    //blocks should not be spawned at posiiton 0,0,0
                    if (targetPos == Vector3.zero)
                    {
                        yield return null;
                        continue;
                    }

                    //if don't have a path or the path need to be recalculated,
                    //skip a turn after
                    if (!_aIController.HasPath() || recalculateFlag)
                    {
                        recalculateFlag = false;
                        _aIController.CalculatePath(bodyParts[0].position, targetPos);
                        yield return null;
                        continue;
                    }

                    //there's a path?
                    if (_aIController.HasPath())
                    {
                        //get a position from the script with the path list
                        Vector3 nextPos = _aIController.GetComponentNextPosition();

                        //calculate the rotation based on the direction of the position
                        Vector3 lookAtPos = new Vector3(nextPos.x, bodyParts[0].transform.position.y, nextPos.z);

                        //if the position is behind the front block, make turns
                        if (lookAtPos == (bodyParts[0].position - bodyParts[0].forward))
                        {
                            lookAtPos = bodyParts[0].position + (directionCorrector * bodyParts[0].right);
                            directionCorrector *= -1;

                            recalculateFlag = true;

                            bodyParts[0].LookAt(lookAtPos);
                        }
                        else
                        {
                            bodyParts[0].LookAt(lookAtPos);
                        }
                    }
                }
                else
                {
                    turnsToSkip--;

                    //always need to recalculate path after finish to skip the turns
                    recalculateFlag = true;
                }

                UpdateHead();

                bodyParts[0].position += bodyParts[0].forward;
                bodyParts[0].GetComponent<ScreenTeleporter>().CheckPosition();

                bodyParts[0].position = RoundPosition(bodyParts[0].position);
                bodyParts[0].eulerAngles = RoundRotation(bodyParts[0].eulerAngles);

                //low sound played with each movement
                stepSound.Play();

                for (int i = 1; i < bodyParts.Count; i++)
                {
                    if (bodyParts[i] == null)
                    {
                        bodyParts.RemoveAt(i);

                        if (bodyParts.Count <= i)
                        {
                            break;
                        }
                    }

                    //just to keep the info
                    _currentBodyPart = bodyParts[i];
                    _previousBodyPart = bodyParts[i - 1];

                    //fix the new position based on the previous part old position
                    Vector3 newPos = _previousPosition;
                    newPos.y = bodyParts[0].position.y;

                    //save info for the next loop

                    if(_currentBodyPart == null)
                    {
                        Debug.LogError(gameObject.name + " has the pos " + i + " null");
                    }

                    _previousPosition = _currentBodyPart.position;
                    _previousRotation = _currentBodyPart.rotation;

                    //keeps track of all the blocks movements, currently used to get info for the time travel,
                    //but in the future could be used for other stuff like an mirror snake
                    _currentBodyPart.GetComponent<BlockBase>().RegisterLastPositionAndRotation(_previousPosition, _previousRotation);

                    //add the new pos and rot one turn behind
                    _currentBodyPart.LookAt(newPos);
                    _currentBodyPart.position = newPos;

                    _currentBodyPart.position = RoundPosition(_currentBodyPart.position);
                    _currentBodyPart.eulerAngles = RoundRotation(_currentBodyPart.eulerAngles);
                }


                yield return new WaitForSeconds(1f / CalculateSpeed(speed));
            }
        }

        protected override void HitVFX(BlockBase head)
        {
            GameObject hitFX = PoolingManager.Instance.UseObject(EffectsHelper.Instance.hitBasic, head.transform.position, Quaternion.identity);
            PoolingManager.Instance.ReturnObject(hitFX, 1f);
        }

        Vector3Int RoundPosition(Vector3 pos)
        {
            return Vector3Int.RoundToInt(pos);
        }

        Vector3Int RoundRotation(Vector3 euler)
        {
            var rot = euler;
            rot.x = 0;
            rot.y = Mathf.Round(rot.y / 90) * 90;
            rot.z = 0;

            return Vector3Int.RoundToInt(rot);
        }

        #endregion

    }
}
