﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Block;
using Core;

namespace Snake
{
    public class SnakeSetupAI : SnakeSetupBase
    {
        SnakeMovementAI _snakeMovementAI;

        #region Unity Functions

        private void Awake()
        {
            _snakeMovement = GetComponent<SnakeMovementBase>();
            _snakeMovementAI = _snakeMovement.GetComponent<SnakeMovementAI>();
        }

        #endregion

        #region Public Functions

        public void SetupAI(SnakeSetupPlayer snakeSetupPlayer)
        {
            //the setup for the AI is connect to a setup for a snake (the nemesis)
            SetNemesis(snakeSetupPlayer.gameObject);

            //used in various situations, 
            //it’s important not to duplicate this information so much
            number = snakeSetupPlayer.number;

            transform.position = BlocksSpawner.Instance.GetRandomGridPosition(10);

            int randomType = Random.Range(0, 4);
            //int randomType = 0; //test

            ConfigureInitialBlockTypes((SnakeTypes)randomType);

            _snakeMovement.AddFirstBlockToBody();

            for (int i = 1; i < initialBlocks.Count; i++)
            {
                _snakeMovement.AddBlockToBody(initialBlocks[i], true);
            }

            BlocksSpawner.Instance.onSpawnBlock += _snakeMovementAI.SetTargetPosition;

            //need to set a point to consider all the initial blocks
            if ((SnakeTypes)randomType == SnakeTypes.ALL || (SnakeTypes)randomType == SnakeTypes.TIME_TRAVEL)
            {
                GameManager.Instance.SetTimeTravelPoint();
            }

            _snakeMovementAI.StartMovement();
        }

        #endregion

        #region Private Functions
        #endregion
    }
}
