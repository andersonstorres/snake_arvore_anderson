﻿using System.Collections;
using UnityEngine;
using Block;
using Core;
using DG.Tweening;

namespace Snake
{
    public class SnakeMovementPlayer : SnakeMovementBase
    {

        bool _newBlockBuffer;

        SnakeScore _snakeScore;
        SnakeSetupPlayer _snakeSetupP;

        Camera _mainCam;

        #region Unity Functions

        private void Awake()
        {
            _snakeSetup = GetComponent<SnakeSetupPlayer>();
            _snakeSetupP = _snakeSetup.GetComponent<SnakeSetupPlayer>();

            _snakeScore = GetComponent<SnakeScore>();
            
        }

        private void Start()
        {
            _mainCam = Camera.main;
        }

        private void Update()
        {
            if (Input.GetKeyDown(_snakeSetupP.leftKey))
            {
                _rotationBuffer = -1f;
            }

            if (Input.GetKeyDown(_snakeSetupP.rightKey))
            {
                _rotationBuffer = 1f;
            }

            if (inIframes)
            {
                if (_iframesLeft > 0)
                {
                    _iframesLeft -= Time.deltaTime;
                }
                else
                {
                    ExitIframes();
                }
            }
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }

        #endregion

        #region Public Functions

        public override void StartMovement()
        {
            EnterIframes(true);

            GameObject ai = GameManager.Instance.AskForAI(this.gameObject); //called first because need to assign listener
            _snakeSetup.SetNemesis(ai);

            BlocksSpawner.Instance.AskForNewBlock(this.gameObject);                       

            _moveCoroutine = StartCoroutine(MoveCo());
        }

        public override void HandleCollision(BlockBase head, BlockBase other)
        {
            if (inIframes)
            {
                return;
            }

            //screen shake effect
            if (!EffectsHelper.Instance.c_shaking)
            {
                EffectsHelper.Instance.c_shaking = true;
                _mainCam.DOShakePosition(EffectsHelper.Instance.c_duration, EffectsHelper.Instance.c_strength).OnComplete(() =>
                {
                    EffectsHelper.Instance.c_shaking = false;
                });
            }

            DebugHelper.Instance.Log(gameObject.name + " block: " + head + " HIT " + other.snake + " block " + other.type, gameObject);
            HitVFX(head);

            #region Time Travel
            BlockBase tempBlock = FindBlockByType(BlockType.TIME_TRAVEL);

            //found a block?
            if (tempBlock != null)
            {
                //he really has the correct script
                TimeTravel tempTime = tempBlock.GetComponent<TimeTravel>();

                if (tempTime != null)
                {
                    EnterIframes();

                    //remove the own block
                    GameManager.Instance.RemoveBlock(bodyParts, tempTime, _snakeScore, 0);

                    //travel in time
                    GameManager.Instance.TimeTravel();

                    return;
                }
            }
            #endregion

            #region Battering Ram            
            tempBlock = FindBlockByType(BlockType.BATTERING_RAM, true);

            //found a block?
            if (tempBlock != null)
            {
                //he really has the correct script?
                BatteringRam tempBat = tempBlock.GetComponent<BatteringRam>();

                if (tempBat != null)
                {
                    //there's snake enough to handle a block destruction?
                    if (bodyParts.Count > 1)
                    {

                        //hit other snake?
                        if (other.snake != head.snake)
                        {
                            //the other block is the head?
                            if (other.IsHead())
                            {
                                //it's not a battering or time travel (priority)
                                if (other.type != BlockType.BATTERING_RAM && other.type != BlockType.TIME_TRAVEL)
                                {
                                    //remove the other block
                                    GameManager.Instance.RemoveBlock(other.snake.bodyParts, other, other.snake.GetComponent<SnakeScore>(), 0.3f);
                                }
                            }
                            else
                            {
                                //remove the other block
                                GameManager.Instance.RemoveBlock(other.snake.bodyParts, other, other.snake.GetComponent<SnakeScore>(), 0.3f);
                            }
                        }
                        //hit iself?
                        else
                        {
                            //is the block that will already be destroyed?
                            if (other != tempBat)
                            {
                                //remove your other block
                                GameManager.Instance.RemoveBlock(other.snake.bodyParts, other, _snakeScore, 0.25f);
                            }
                        }

                        //remove the own block
                        GameManager.Instance.RemoveBlock(bodyParts, tempBat, _snakeScore, 0.2f);

                        EnterIframes();

                        return;
                    }
                }
            }
            #endregion

            Die();
        }

        public override void Die()
        {
            base.Die();

            GameManager.Instance.RemoveSnake(_snakeSetup.GetNemesis(), false); //remove the ai for this player
            GameManager.Instance.RemoveSnake(this.gameObject, true);
        }

        //when an snake dies, destroys all the collectables loose blocks just before the snake be removed
        public void CallForDestruction()
        {
            BlockBase[] blocks = FindObjectsOfType<BlockBase>();
            for(int i = 0; i < blocks.Length; i++)
            {
                if(blocks[i].originalSnake == this)
                {
                    blocks[i].DestroyWithTheSnake();
                }
            }
        }

        #endregion

        #region Private Functions

        IEnumerator MoveCo()
        {
            while (true && !Dead)
            {
                //still has a body?
                if (bodyParts.Count <= 0) break;

                //any time travel occuring?
                if (GameManager.Instance.Traveling)
                {
                    yield return null;
                    continue;
                }

                //need to spawn some block? (not currently in use, but could be in the future)
                //if (_newBlockBuffer)
                //{
                //    AddBlockToBody();
                //    _newBlockBuffer = false;
                //}

                //something goes wrong with the first element?
                if(bodyParts[0] == null)
                {
                    DebugHelper.Instance.LogError("No block on position 0", gameObject);
                    yield return null;
                    continue;
                }

                //caches the current transform state of the the fist block
                _previousPosition = bodyParts[0].position;
                _previousRotation = bodyParts[0].rotation;

                //keeps track of all the blocks movements, currently used to get info for the time travel,
                //but in the future could be used for other stuff like an mirror snake
                bodyParts[0].GetComponent<BlockBase>().RegisterLastPositionAndRotation(_previousPosition, _previousRotation);

                if (_rotationBuffer != 0f)
                {
                    bodyParts[0].Rotate(Vector3.up * 90f * _rotationBuffer);
                    _rotationBuffer = 0f;
                }

                UpdateHead();

                bodyParts[0].position += bodyParts[0].forward;
                bodyParts[0].GetComponent<ScreenTeleporter>().CheckPosition();

                //low sound played with each movement
                stepSound.Play();

                for (int i = 1; i < bodyParts.Count; i++)
                {
                    if (bodyParts[i] == null)
                    {
                        bodyParts.RemoveAt(i);

                        if (bodyParts.Count <= i)
                        {
                            break;
                        }
                    }

                    //just to keep the info
                    _currentBodyPart = bodyParts[i];
                    _previousBodyPart = bodyParts[i - 1];

                    //fix the new position based on the previous part old position
                    Vector3 newPos = _previousPosition;
                    newPos.y = bodyParts[0].position.y;

                    //save info for the next loop
                    _previousPosition = _currentBodyPart.position;
                    _previousRotation = _currentBodyPart.rotation;

                    //keeps track of all the blocks movements, currently used to get info for the time travel,
                    //but in the future could be used for other stuff like an mirror snake
                    _currentBodyPart.GetComponent<BlockBase>().RegisterLastPositionAndRotation(_previousPosition, _previousRotation);

                    //add the new pos and rot one turn behind
                    _currentBodyPart.LookAt(newPos);
                    _currentBodyPart.position = newPos;
                    
                }

                yield return new WaitForSeconds(1f / CalculateSpeed(speed));
            }
        }

        protected override void SetupBlockOnSnake(Transform block, BlockBase blockBase)
        {
            base.SetupBlockOnSnake(block, blockBase);

            blockBase.TintBlock();
        }

        protected override void HitVFX(BlockBase head)
        {
            GameObject hitFX = PoolingManager.Instance.UseObject(EffectsHelper.Instance.hitBasic, head.transform.position, Quaternion.identity);
            PoolingManager.Instance.ReturnObject(hitFX, 1f);
        }

        #endregion
    }
}
