﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Block;
using Core;

namespace Snake
{
    public class SnakeSetupPlayer : SnakeSetupBase
    {
        public KeyCode leftKey;
        public KeyCode rightKey;

        #region Unity Functions

        private void Awake()
        {
            _snakeMovement = GetComponent<SnakeMovementBase>();
        }

        #endregion

        #region Public Functions

        public void SetupPlayer(GameManager.PlayerSetup playerSetup)
        {
            //used in various situations, 
            //it’s important not to duplicate this information so much
            number = playerSetup.number;

            //random int position (1 unit) inside the availables
            transform.position = BlocksSpawner.Instance.GetRandomGridPosition(5);

            leftKey = playerSetup.leftKey;
            rightKey = playerSetup.rightKey;

            ConfigureInitialBlockTypes(playerSetup.initialSnake);

            _snakeMovement.AddFirstBlockToBody();

            for (int i = 1; i < initialBlocks.Count; i++)
            {
                _snakeMovement.AddBlockToBody(initialBlocks[i], true);
            }

            //need to set a point to consider all the initial blocks
            if (playerSetup.initialSnake == SnakeTypes.ALL || playerSetup.initialSnake == SnakeTypes.TIME_TRAVEL)
            {
                GameManager.Instance.SetTimeTravelPoint();
            }

            _snakeMovement.StartMovement();
        }

        #endregion

        #region Private Functions
        #endregion

    }
}
