﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

namespace Snake
{
    public class ScreenTeleporter : MonoBehaviour
    {
        #region Unity Functions

        #endregion

        #region Public Functions

        //the classic effect of disappear and reappear on the oposite side of the screen
        public void CheckPosition()
        {
            if(transform.position.x < BlocksSpawner.Instance.topLeft.position.x)
            {
                //teleport horizontal left
                Vector3 newPos = new Vector3(BlocksSpawner.Instance.bottomRight.position.x, 0f, transform.position.z);
                transform.position = newPos;
            }
            else
            if (transform.position.x > BlocksSpawner.Instance.bottomRight.position.x)
            {
                //teleport horizontal right
                Vector3 newPos = new Vector3(BlocksSpawner.Instance.topLeft.position.x, 0f, transform.position.z);
                transform.position = newPos;
            }
            else
            if (transform.position.z < BlocksSpawner.Instance.bottomRight.position.z)
            {
                //teleport vertical bottom
                Vector3 newPos = new Vector3(transform.position.x, 0f, BlocksSpawner.Instance.topLeft.position.z);
                transform.position = newPos;
            }
            else
            if (transform.position.z > BlocksSpawner.Instance.topLeft.position.z)
            {
                //teleport vertical top
                Vector3 newPos = new Vector3(transform.position.x, 0f, BlocksSpawner.Instance.bottomRight.position.z);
                transform.position = newPos;
            }
        }

        #endregion

        #region Private Functions

        #endregion
    }
}
