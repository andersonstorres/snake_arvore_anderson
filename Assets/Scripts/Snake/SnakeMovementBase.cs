﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Block;
using Core;
using System;
using System.Linq;

namespace Snake
{
    public class SnakeMovementBase : MonoBehaviour
    {
        [Header("Movement")]
        [SerializeField] protected float speed = 3f;
        [SerializeField] protected AudioSource stepSound;
        [SerializeField] protected float blockSpeedReduction = 0.2f;

        [Header("Body")]
        [SerializeField] protected Transform blocksHolder;
        public List<Transform> bodyParts = new List<Transform>();

        [Header("IFrames")]
        public bool inIframes;
        protected float _iframesLeft;
        public event Action onEnterIframes;
        public event Action onExitIframes;

        protected Transform _currentBodyPart;
        protected Transform _previousBodyPart;
        protected Vector3 _previousPosition;
        protected Quaternion _previousRotation;
        protected float _rotationBuffer = 0f;
        protected Vector3 _lastTimeTravelPosition;
        protected Quaternion _lastTimeTravelRotation;

        protected Coroutine _moveCoroutine;
        protected SnakeSetupBase _snakeSetup;

        public bool dead;

        protected bool Dead { get => dead; private set => dead = value; }

        #region Unity Functions

        #endregion

        #region Public Functions

        public virtual int GetNumber() 
        {
            return _snakeSetup.number;
        }
        public virtual void StartMovement() { }

        //first block have some changes on setup
        public virtual void AddFirstBlockToBody() 
        {
            Transform firstBlock = (Instantiate(BlocksSpawner.Instance.GetBlockPrefabByType(_snakeSetup.initialBlocks[0]), blocksHolder) as GameObject).transform;

            bodyParts.Add(firstBlock);

            BlockBase firstBlockBase = firstBlock.GetComponent<BlockBase>();

            firstBlockBase.GrowScale();

            SetupBlockOnSnake(firstBlock, firstBlockBase);

        }

        public virtual void AddBlockToBody(BlockType blockType = BlockType.NONE, bool init = false) 
        {
            GameObject blockPrefab = null;

            //could be used in the future for special collectable blocks
            if (blockType == BlockType.NONE)
            {
                blockPrefab = BlocksSpawner.Instance.GetRandomBlockPrefab();
            }
            else
            {
                blockPrefab = BlocksSpawner.Instance.GetBlockPrefabByType(blockType);
            }

            Transform newBlock = (Instantiate(blockPrefab,
                bodyParts[0].position + bodyParts[0].forward, //set in front of the front
                bodyParts[0].rotation) as GameObject).transform; //set with the same rotation as the front block

            newBlock.SetParent(blocksHolder);

            BlockBase newBlockBase = newBlock.GetComponent<BlockBase>();
            BlockBase currentBlockBase = bodyParts[0].GetComponent<BlockBase>();

            //insert as the first item of a new list and the change the current list
            List<Transform> tempList = bodyParts.Prepend(newBlock.transform).ToList();
            bodyParts = tempList;

            if (!init)
            {
                newBlockBase.PunchScale();
            }
            else
            {
                newBlockBase.GrowScale();
            }

            SetupBlockOnSnake(newBlock, newBlockBase);
        }

        public virtual void HandleCollision(BlockBase head, BlockBase other) { }

        public virtual BlockBase FindBlockByType(BlockType blockType, bool reverse = false)
        {
            //probably you want to get the last battering ram from the snake body
            if (!reverse)
            {
                for (int i = 0; i < bodyParts.Count; i++)
                {
                    if (bodyParts[i] != null && bodyParts[i].GetComponent<BlockBase>().type == blockType)
                    {
                        return bodyParts[i].GetComponent<BlockBase>();
                    }
                }
            }
            else 
            {
                for (int i = bodyParts.Count - 1; i >= 0; i--)
                {
                    if (bodyParts[i] != null && bodyParts[i].GetComponent<BlockBase>().type == blockType)
                    {
                        return bodyParts[i].GetComponent<BlockBase>();
                    }
                }
            }            

            return null;
        }

        //dies only once
        public virtual void Die() 
        {
            if (Dead) return;

            Dead = true;
        }

        //accesses the other SnakeSetupBase script to keep information in one place
        public GameObject GetNemesis()
        {
            return _snakeSetup.GetNemesis();
        }

        //called before every movement to be certainty that correct movement will be performed
        public virtual void UpdateHead()
        {
            for (int i = 0; i < bodyParts.Count; i++)
            {
                if(i == 0)
                {
                    if (bodyParts[i] != null)
                    {
                        bodyParts[i].GetComponent<BlockBase>().SetHead(true);
                    }
                }
                else
                {
                    if (bodyParts[i] != null)
                    {
                        bodyParts[i].GetComponent<BlockBase>().SetHead(false);
                    }
                }
            }
        }

        //sometimes a block could be removed in a diferent situation that a collision, 
        //this checks if the snake still have blocks
        public void CheckDeath()
        {
            if (bodyParts.Count <= 0)
            {
                Die();
            }
        }

        #endregion

        #region Private Functions

        protected void EnterIframes(bool start = false)
        {
            if (!GameManager.Instance.GetUseIframes() && start) return;

            if (!inIframes)
            {
                inIframes = true;
                _iframesLeft = GameManager.Instance.GetIframesSeconds();
                onEnterIframes?.Invoke();
            }
        }

        protected void ExitIframes()
        {
            if (inIframes)
            {
                inIframes = false;
                onExitIframes?.Invoke();
            }
        }

        //configure the block on the snake, has some extra configurations on SnakeMovementPlayer
        protected virtual void SetupBlockOnSnake(Transform block, BlockBase blockBase)
        {
            blockBase.RegisterLastPositionAndRotation(block.position, block.rotation);
            blockBase.SetSnake(this.gameObject);
            blockBase.ActivatePassiveEffect();
            blockBase.ToggleCover(true);            
        }
        
        //speed affects the delay between every snake movement
        protected virtual float CalculateSpeed(float speed)
        {
            float partsSpeed = speed - (blockSpeedReduction * bodyParts.Count);
            float finalSpeed = speed + _snakeSetup.ExtraEnginePower;
            return Mathf.Max(finalSpeed, 0.5f);
        }

        protected virtual void HitVFX(BlockBase head) { }

        #endregion
    }
}
