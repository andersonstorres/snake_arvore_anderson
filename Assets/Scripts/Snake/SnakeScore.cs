﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Block;
using Core;
using Score;

namespace Snake
{
    public class SnakeScore : MonoBehaviour
    {
        int score, engineQty, timeTravelQty, batteringRamQty;

        public int Score { get => score; private set => score = value; }
        public int EngineQty { get => engineQty; private set => engineQty = value; }
        public int TimeTravelQty { get => timeTravelQty; private set => timeTravelQty = value; }
        public int BatteringRamQty { get => batteringRamQty; private set => batteringRamQty = value; }

        SnakeSetupPlayer snakeSetup;

        #region Unity Functions

        private void Awake()
        {
            snakeSetup = GetComponent<SnakeSetupPlayer>();
        }

        #endregion

        #region Public Functions

        public int GetNumber()
        {
            return snakeSetup.number;
        }

        public KeyCode GetLeftKey()
        {
            return snakeSetup.leftKey;
        }

        public KeyCode GetRightKey()
        {
            return snakeSetup.rightKey;
        }

        public void AddScore(int qty = 1)
        {
            Score += qty;

            ScoreManager.Instance.UpdateScore(snakeSetup.number - 1);
        }

        public void AddBlock(BlockType blockType, int qty = 1)
        {
            switch (blockType)
            {
                case BlockType.ENGINE_POWER:

                    EngineQty += qty;

                    break;
                case BlockType.TIME_TRAVEL:

                    TimeTravelQty += qty;

                    break;
                case BlockType.BATTERING_RAM:

                    BatteringRamQty += qty;

                    break;
            }

            ScoreManager.Instance.UpdateScore(snakeSetup.number - 1);
        }

        public void RemoveBlock(BlockType blockType, int qty = 1)
        {
            switch (blockType)
            {
                case BlockType.ENGINE_POWER:

                    EngineQty -= qty;

                    break;
                case BlockType.TIME_TRAVEL:

                    TimeTravelQty -= qty;

                    break;
                case BlockType.BATTERING_RAM:

                    BatteringRamQty -= qty;

                    break;
            }

            ScoreManager.Instance.UpdateScore(snakeSetup.number - 1);
        }

        #endregion

        #region Private Functions
        #endregion
    }
}