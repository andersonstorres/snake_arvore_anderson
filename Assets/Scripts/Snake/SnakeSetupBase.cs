﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Block;
using Core;
using System;

namespace Snake
{
    public class SnakeSetupBase : MonoBehaviour
    {
        [Tooltip("Display number, decrease 1 to get index")]
        public int number;

        public List<BlockType> initialBlocks = new List<BlockType>();

        [Header("Powers")]
        [SerializeField] protected float extraEnginePower = 0f;
        [SerializeField] private int enginePower = 0;
        [SerializeField] protected int timeTravels = 0;
        [SerializeField] protected int batteringRams = 0;

        GameObject _nemesis;

        public float ExtraEnginePower { get => extraEnginePower; protected set => extraEnginePower = value; }
        protected int EnginePower { get => enginePower; set => enginePower = value; }
        public int TimeTravels { get => timeTravels; protected set => timeTravels = value; }
        public int BatteringRams { get => batteringRams; protected set => batteringRams = value; }
        

        protected SnakeMovementBase _snakeMovement;

        #region Unity Functions
        #endregion

        #region Public Functions

        //add to keep track of the current quantity of each type of block 
        public void AddToCountByType(BlockType type)
        {
            switch (type)
            {
                case BlockType.ENGINE_POWER:

                    AddEnginePower();

                    break;
                case BlockType.TIME_TRAVEL:

                    AddTimeTravel();

                    break;
                case BlockType.BATTERING_RAM:

                    AddBatteringRam();

                    break;
            }
        }

        //remove to keep track of the current quantity of each type of block 
        public void RemoveFromCountByType(BlockType type)
        {
            switch (type)
            {
                case BlockType.ENGINE_POWER:

                    RemoveEnginePower();

                    break;
                case BlockType.TIME_TRAVEL:

                    RemoveTimeTravel();

                    break;
                case BlockType.BATTERING_RAM:

                    RemoveBatteringRam();

                    break;
            }
        }

        //the extra speed is acumulated here avoiding some calculations
        public virtual void PowerToEngine(float value)
        {
            ExtraEnginePower += value;
        }

        public virtual void RemovePowerFromEngine(float value)
        {
            ExtraEnginePower -= value;

            if (ExtraEnginePower < 0f)
            {
                DebugHelper.Instance.LogError("Negative power", gameObject);
            }
        }

        protected virtual void AddEnginePower()
        {
            EnginePower++;
        }

        protected virtual void RemoveEnginePower()
        {
            EnginePower--;

            if(EnginePower < 0)
            {
                DebugHelper.Instance.LogError("Negative engines", gameObject);
            }
        }

        protected virtual void AddTimeTravel()
        {
            TimeTravels++;
        }

        protected virtual void RemoveTimeTravel()
        {
            TimeTravels--;

            if (TimeTravels < 0)
            {
                DebugHelper.Instance.LogError("Negative time travels", gameObject);
            }
        }

        protected virtual void AddBatteringRam()
        {
            BatteringRams++;
        }

        protected virtual void RemoveBatteringRam()
        {
            BatteringRams--;

            if (BatteringRams < 0)
            {
                DebugHelper.Instance.LogError("Negative battering ram", gameObject);
            }
        }

        //nemesis are the snake's contrapart (AIs for players, and player for AIs), 
        //important to compare block informations, AI lifespan and other stuff
        public GameObject GetNemesis()
        {
            if(_nemesis != null)
            {
                return _nemesis;
            }

            if(gameObject.TryGetComponent(out SnakeSetupPlayer player))
            {
                //in case of an player doesn't have an nemesis AI, ask for a new one
                GameManager.Instance.AskForAI(this.gameObject);
            }
            else if(gameObject.TryGetComponent(out SnakeSetupAI ai))
            {
                //in case of an AI doesn't have an namesis player, destroy itself
                GameManager.Instance.RemoveSnake(this.gameObject, false);
            }

            return null;
        }

        public void SetNemesis(GameObject nemesis)
        {
            _nemesis = nemesis;
        }

        #endregion

        #region Private Functions
        protected void ConfigureInitialBlockTypes(SnakeTypes initial)
        {
            //based on the initial block setup, add the 
            //corresponding blocks to the snake
            switch (initial)
            {
                case SnakeTypes.ALL:

                    initialBlocks.Add(BlockType.TIME_TRAVEL);
                    initialBlocks.Add(BlockType.BATTERING_RAM);                    
                    initialBlocks.Add(BlockType.ENGINE_POWER);                    

                    break;
                case SnakeTypes.ENGINE:

                    initialBlocks.Add(BlockType.ENGINE_POWER);
                    initialBlocks.Add(BlockType.ENGINE_POWER);
                    initialBlocks.Add(BlockType.ENGINE_POWER);

                    break;
                case SnakeTypes.TIME_TRAVEL:

                    initialBlocks.Add(BlockType.TIME_TRAVEL);
                    initialBlocks.Add(BlockType.TIME_TRAVEL);
                    initialBlocks.Add(BlockType.TIME_TRAVEL);

                    break;
                case SnakeTypes.BATTERING_RAM:

                    initialBlocks.Add(BlockType.BATTERING_RAM);
                    initialBlocks.Add(BlockType.BATTERING_RAM);
                    initialBlocks.Add(BlockType.BATTERING_RAM);

                    break;
            }
        }

        #endregion
    }
}
