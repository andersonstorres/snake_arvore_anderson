﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace UI
{
    public class EliminatedMsg : MonoBehaviour
    {
        public static EliminatedMsg Instance;

        [SerializeField] GameObject msgHolder;
        [SerializeField] TextMeshProUGUI msg;

        #region Unity Functions

        private void Awake()
        {
            Instance = this;
        }

        #endregion

        #region Public Functions

        public void ShowMessage(int playerNumber, float duration = 0f)
        {
            msgHolder.SetActive(true);
            msg.text = "<color=white>P" + playerNumber + "</color> no longer exists ...";

            StartCoroutine(Hide(duration == 0f ? 1f : duration));
        }

        public void ForceHide()
        {
            StopAllCoroutines();
            msgHolder.SetActive(false);
        }

        #endregion

        #region Private Functions

        IEnumerator Hide(float duration)
        {
            yield return new WaitForSeconds(duration);

            msgHolder.SetActive(false);
        }


        #endregion
    }
}
