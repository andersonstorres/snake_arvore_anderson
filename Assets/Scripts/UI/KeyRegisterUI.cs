﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using Core;
using Audio;

namespace UI
{
    public class KeyRegisterUI : MonoBehaviour
    {
        public KeyCode selectedKey;

        [SerializeField] TextMeshProUGUI keyText;
        [SerializeField] float holdTime = 1f;
        [SerializeField] Image holdBar;

        public UnityEvent onKeySelect;

        float timer = 0f;
        KeyCode tempKey;

        PlayerSetupUI playerSetupUI;
        PlayerEnterUI playerEnterUI;

        bool confirming;
        bool tinted;

        #region Unity Functions

        private void OnEnable()
        {
            playerSetupUI = GetComponentInParent<PlayerSetupUI>();
            playerEnterUI = FindObjectOfType<PlayerEnterUI>();
            holdBar.fillAmount = 0;            
        }

        private void Update()
        {
            WaitForKey();
        }

        #endregion

        #region Public Functions
        #endregion

        #region Private Functions

        void WaitForKey()
        {
            foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKey(vKey))
                {
                    if (playerEnterUI.ValidateKey(vKey) && !confirming)
                    {
                        TintHoldBar();

                        timer += Time.deltaTime;
                        tempKey = vKey;
                        keyText.text = GameManager.Instance.FormatKey(vKey);
                        holdBar.fillAmount = Mathf.Clamp(timer, 0f, 1f);

                        if (timer >= holdTime && !confirming)
                        {
                            StartCoroutine(ConfirmKeyCo(vKey));
                        }
                    }
                    else
                    {
                        if (!confirming)
                        {
                            keyText.text = "Hold key";
                            holdBar.fillAmount = 0;
                        }
                    }
                }

                if (Input.GetKeyUp(vKey))
                {
                    holdBar.fillAmount = 0;
                    timer = 0;
                }
            }
        }

        IEnumerator ConfirmKeyCo(KeyCode vKey)
        {
            confirming = true;
            selectedKey = vKey;
            keyText.text = GameManager.Instance.FormatKey(vKey);
            playerSetupUI.SetKey(vKey);

            AudioController.Instance.PlayAudio(Audio.AudioType.UI_REGISTER_KEY);

            VirtualKeyboard.Instance.TintKey(vKey, GameManager.Instance.colorScheme.GetColorByIndex(playerSetupUI.playerNumber - 1));

            yield return new WaitForSeconds(.5f);

            onKeySelect?.Invoke();
        }

        void TintHoldBar()
        {
            if (tinted) return;

            holdBar.color = GameManager.Instance.colorScheme.GetColorByIndex(playerSetupUI.playerNumber - 1);

            tinted = true;
        }

        #endregion
    }
}
