﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Core;
using Audio;

namespace UI
{
    public enum Panel
    {
        NONE, TITLE, PLAYERS
    }

    public class MainMenuUI : MonoBehaviour
    {
        public MenuPanel currentPanel;

        [SerializeField] List<MenuPanel> menuPanels = new List<MenuPanel>();
        [SerializeField] Button startGameButton;

        [System.Serializable]
        public class MenuPanel
        {
            public Panel panelTitle;
            public GameObject panelObject;
        }

        #region Unity Functions

        private void Start()
        {
            HideAllPanels();
            ChangePanel(Panel.TITLE);
            AddSoundToButtons();
        }

        private void Update()
        {
            switch (currentPanel.panelTitle)
            {
                case Panel.NONE:
                    break;
                case Panel.TITLE:

                    WaitForAnyKey();
                    startGameButton.interactable = false;
                    startGameButton.GetComponentInChildren<PulseUI>().Stop();

                    break;
                case Panel.PLAYERS:

                    if (Input.GetKey(KeyCode.Escape))
                    {
                        ReturnToTitleScreen();
                    }

                    if(GameManager.Instance.playerSetups.Count > 0)
                    {
                        startGameButton.interactable = true;
                        startGameButton.GetComponentInChildren<PulseUI>().Pulse();
                    }
                    else
                    {
                        startGameButton.interactable = false;
                        startGameButton.GetComponentInChildren<PulseUI>().Stop();
                    }

                    break;
            }
        }

        public void AssignStartGameFuntion()
        {
            GameManager.Instance.StartGame();
        }

        #endregion

        #region Public Functions

        public void ChangePanel(Panel enter)
        {
            MenuPanel enterPanel = GetPanel(enter);     
            
            if(currentPanel.panelObject != null)
            {
                currentPanel.panelObject.SetActive(false);
            }

            if(enterPanel != null)
            {
                enterPanel.panelObject.SetActive(true);

                currentPanel = enterPanel;
            }
        }

        public void HideAllPanels()
        {
            foreach (MenuPanel p in menuPanels)
            {
                if (p.panelObject != null)
                {
                    p.panelObject.SetActive(false);
                }
            }

            currentPanel = GetPanel(Panel.NONE);
        }

        #endregion

        #region Private Functions

        MenuPanel GetPanel(Panel panel)
        {
            foreach(MenuPanel p in menuPanels)
            {
                if(p.panelTitle == panel)
                {
                    return p;
                }
            }

            return null;
        }


        void WaitForAnyKey()
        {
            if (Input.anyKeyDown)
            {
                //Now check if your key is being pressed
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    GameManager.Instance.Quit();
                }
                else
                {
                    AudioController.Instance.PlayAudio(Audio.AudioType.UI_BUTTON_CLICK);
                    ChangePanel(Panel.PLAYERS);
                }               
            }
        }

        void ReturnToTitleScreen()
        {
            PlayerEnterUI playerEnterUI = GetComponentInChildren<PlayerEnterUI>();

            if(playerEnterUI != null)
            {
                GameManager.Instance.playerSetups.Clear();
                playerEnterUI.DestroyAllSetups();
                ChangePanel(Panel.TITLE);
            }
        }

        void AddSoundToButtons()
        {
            foreach (Button btn in GetComponentsInChildren(typeof(Button), true))
            {
                btn.onClick.AddListener(() => 
                {
                    AudioController.Instance.PlayAudio(Audio.AudioType.UI_BUTTON_CLICK);
                });
            }
        }

        #endregion
    }
}
