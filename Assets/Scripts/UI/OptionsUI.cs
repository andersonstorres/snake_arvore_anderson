﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using TMPro;
using UnityEngine.UI;

namespace UI
{
    public class OptionsUI : MonoBehaviour
    {

        [SerializeField] GameObject optionsPanel;
        [SerializeField] GameObject iframeCheck;
        [SerializeField] GameObject aiPowerCheck;
        [SerializeField] TextMeshProUGUI difficultyText;
        [SerializeField] Button leftButton, rightButton;

        int difficultiesQty;
        int currentDifficultyIndex = 0;

        #region Unity Functions

        private void Start()
        {
            ToggleOptions(false);

            UpdateAI();
        }

        #endregion

        #region Public Functions

        public void ToggleOptions()
        {
            optionsPanel.SetActive(!optionsPanel.activeInHierarchy);
        }

        public void ToggleOptions(bool enable)
        {
            optionsPanel.SetActive(enable);
        }

        public void ToggleIFrames()
        {
            if (GameManager.Instance.GetUseIframes() == true)
            {
                iframeCheck.gameObject.SetActive(false);
                GameManager.Instance.SetUseIframes(false);
            }
            else
            {
                iframeCheck.gameObject.SetActive(true);
                GameManager.Instance.SetUseIframes(true);
            }

            GameManager.Instance.SaveOptionsPrefs();
        }

        public void ToggleAIUsePowers()
        {
            if (GameManager.Instance.GetAIUsePowers() == true)
            {
                aiPowerCheck.gameObject.SetActive(false);
                GameManager.Instance.SetAIUsePowers(false);
            }
            else
            {
                aiPowerCheck.gameObject.SetActive(true);
                GameManager.Instance.SetAIUsePowers(true);
            }

            GameManager.Instance.SaveOptionsPrefs();
        }

        public void ChangeDifficulty(int direction)
        {
            currentDifficultyIndex += direction;

            // loop
            //if(currentDifficultyIndex < 0)
            //{
            //    currentDifficultyIndex = difficultiesQty - 1;
            //}
            //else
            //if(currentDifficultyIndex >= difficultiesQty)
            //{
            //    currentDifficultyIndex = 0;
            //}

            // disable arrows
            if(currentDifficultyIndex == 0)
            {
                leftButton.interactable = false;
            }
            else
            {
                leftButton.interactable = true;
            }

            if (currentDifficultyIndex == difficultiesQty - 1)
            {
                rightButton.interactable = false;
            }
            else
            {
                rightButton.interactable = true;
            }

            GameManager.Instance.SetDifficulty((Difficulty)currentDifficultyIndex);
            difficultyText.text = GameManager.Instance.GetDifficulty().ToString();

            GameManager.Instance.SaveOptionsPrefs();
        }

        #endregion

        #region Private Functions

        void UpdateAI()
        {
            difficultyText.text = GameManager.Instance.GetDifficulty().ToString();
            difficultiesQty = System.Enum.GetValues(typeof(Difficulty)).Length;
            currentDifficultyIndex = (int)GameManager.Instance.GetDifficulty();

            iframeCheck.gameObject.SetActive(GameManager.Instance.GetUseIframes());
            aiPowerCheck.gameObject.SetActive(GameManager.Instance.GetAIUsePowers());
            currentDifficultyIndex = (int)GameManager.Instance.GetDifficulty();
            difficultyText.text = GameManager.Instance.GetDifficulty().ToString();
        }

        #endregion
    }
}
