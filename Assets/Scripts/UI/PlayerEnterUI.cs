﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

namespace UI {
    public class PlayerEnterUI : MonoBehaviour
    {
        public List<PlayerSetupUI> playerSetupUIs = new List<PlayerSetupUI>();

        [SerializeField] GameObject playerSetupPrefab;
        [SerializeField] Transform playerSetupHolder;

        [SerializeField] KeysAllowed keysAllowed;
        [SerializeField] List<KeyCode> keysInUse = new List<KeyCode>();

        #region Unity Functions

        private void OnEnable()
        {
            DestroyAllSetups();

            AddPlayer();
        }

        #endregion

        #region Public Functions

        public void AddPlayer()
        {
            if (playerSetupUIs.Count >= GameManager.Instance.colorScheme.GetColorCount()) return;

            GameObject newPlayerPanel = Instantiate(playerSetupPrefab, playerSetupHolder);
            PlayerSetupUI setup = newPlayerPanel.GetComponent<PlayerSetupUI>();            

            playerSetupUIs.Add(setup);            

            setup.Customize(playerSetupUIs.Count - 1);
        }

        public bool ValidateKey(KeyCode key)
        {
            if(keysAllowed.CheckIfAllowed(key) && !keysInUse.Contains(key))
            {
                return true;
            }

            return false;
        }

        public void AddKeyInUse(KeyCode key)
        {
            if (!keysInUse.Contains(key))
            {
                keysInUse.Add(key);
            }
            else
            {
                DebugHelper.Instance.LogError("Duplicate key allowed!", gameObject);
            }
        }

        public List<KeyCode> CopyKeyList()
        {
            return keysAllowed.keys;
        }

        public void DestroyAllSetups()
        {
            foreach (Transform child in playerSetupHolder)
            {
                Destroy(child.gameObject);
            }

            playerSetupUIs.Clear();
            keysInUse.Clear();
        }

        #endregion

        #region Private Functions

        #endregion
    }
}