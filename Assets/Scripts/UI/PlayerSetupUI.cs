﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Core;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
public enum SubPanel
{
    NONE, LEFT_KEY, RIGHT_KEY, SNAKE_SELECT
}

namespace UI
{
    public class PlayerSetupUI : MonoBehaviour
    {
        public MenuSubPanel currentSubPanel;

        [SerializeField] TextMeshProUGUI playerNumberText;
        [SerializeField] TextMeshProUGUI playerBestScore;
        [SerializeField] List<MenuSubPanel> menuSubPanels = new List<MenuSubPanel>();

        public int playerNumber;

        public KeyCode leftKey { get; private set; }
        public KeyCode rightKey { get; private set; }

        PlayerEnterUI playerEnterUI;

        [System.Serializable]
        public class MenuSubPanel
        {
            public SubPanel subPanelTitle;
            public GameObject subPanelObject;
        }

        #region Unity Functions

        private void OnEnable()
        {
            playerEnterUI = GetComponentInParent<PlayerEnterUI>();

            HideAllPanels();
            ChangeSubPanel(SubPanel.LEFT_KEY);
        }

        private void Update()
        {
            switch (currentSubPanel.subPanelTitle)
            {
                case SubPanel.NONE:
                    break;
                case SubPanel.LEFT_KEY:
                    break;
                case SubPanel.RIGHT_KEY:
                    break;
                case SubPanel.SNAKE_SELECT:
                    break;
            }
        }

        #endregion

        #region Public Functions

        public void ChangeSubPanel(SubPanel enter)
        {
            MenuSubPanel enterPanel = GetSubPanel(enter);

            if (currentSubPanel.subPanelObject != null)
            {
                currentSubPanel.subPanelObject.SetActive(false);
            }

            if (enterPanel != null)
            {
                enterPanel.subPanelObject.SetActive(true);

                currentSubPanel = enterPanel;
            }
        }

        //overload to inspector
        public void ChangeSubPanel(int enter_index)
        {
            SubPanel enter = (SubPanel)enter_index;

            ChangeSubPanel(enter);
        }

        public void SetKey(KeyCode keyCode)
        {
            if(currentSubPanel.subPanelTitle == SubPanel.LEFT_KEY)
            {
                leftKey = keyCode;
                playerEnterUI.AddKeyInUse(keyCode);
            }
            else
            if (currentSubPanel.subPanelTitle == SubPanel.RIGHT_KEY)
            {
                rightKey = keyCode;
                playerEnterUI.AddKeyInUse(keyCode);

                StartCoroutine(AllowNewPlayerEnter());
            }
        }

        IEnumerator AllowNewPlayerEnter()
        {
            yield return new WaitForSeconds(1f);
            playerEnterUI.AddPlayer();
        }

        public void Customize(int index)
        {
            GetComponent<UIGridRenderer>().color = GameManager.Instance.colorScheme.GetColorByIndex(index);

            playerNumber = index + 1;

            playerNumberText.text = "P" + (index + 1);

            playerBestScore.text = "Best: " + (PlayerPrefs.GetInt("bestScore" + index, 0)).ToString("0000");

            foreach(TextMeshProUGUI text in GetComponentsInChildren(typeof(TextMeshProUGUI), true))
            {
                text.color = GameManager.Instance.colorScheme.GetColorByIndex(index);
            }
        }

        public void SetPlayer(SnakeTypes currentSelectedType)
        {
            GameManager.Instance.AddOrUpdatePlayer(playerNumber, currentSelectedType, leftKey, rightKey);
        }

        public void ResetScore()
        {
            Debug.Log("reset");
            PlayerPrefs.SetInt("bestScore" + (playerNumber - 1), 0);
            playerBestScore.text = "Best: " + (PlayerPrefs.GetInt("bestScore" + (playerNumber -1), 0)).ToString("0000");
        }

        #endregion

        #region Private Functions

        MenuSubPanel GetSubPanel(SubPanel subPanel)
        {
            foreach (MenuSubPanel p in menuSubPanels)
            {
                if (p.subPanelTitle == subPanel)
                {
                    return p;
                }
            }

            return null;
        }

        void HideAllPanels()
        {
            foreach (MenuSubPanel p in menuSubPanels)
            {
                if (p.subPanelObject != null)
                {
                    p.subPanelObject.SetActive(false);
                }
            }

            currentSubPanel = GetSubPanel(SubPanel.NONE);
        }

        #endregion
    }
}
