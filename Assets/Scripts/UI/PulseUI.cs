﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

namespace UI
{
    public class PulseUI : MonoBehaviour
    {
        [SerializeField] float minScale = 0.5f;
        [SerializeField] float maxScale = 1;
        [SerializeField] float duration = 0.5f;
        [SerializeField] bool activeOnEnable = true;
        [SerializeField] bool playing = false;
        [SerializeField] bool singlePulse = false;

        bool pulsing;

        Tween pulseTween;

        #region Unity Functions

        private void OnEnable()
        {
            if (singlePulse) return;

            pulseTween = transform.DOScale(minScale, duration).From(maxScale).SetLoops(Int32.MaxValue, LoopType.Yoyo);

            pulseTween.Pause();

            if(activeOnEnable || playing)
            {
                Pulse();
            }
            else
            {
                playing = false;
            }
        }

        #endregion

        #region Public Functions

        public void Pulse()
        {
            playing = true;
            pulseTween.Play();
        }

        public void Stop()
        {
            playing = false;
            pulseTween.Pause();
        }

        public void SinglePulse()
        {
            if (pulsing) return;

            pulsing = true;

            transform.DOScale(minScale, duration).From(maxScale).OnComplete(() => 
            {
                transform.DOScale(maxScale, duration).From(minScale).OnComplete(() => 
                {
                    pulsing = false;
                });
            });
        }

        #endregion

        #region Private Functions
        #endregion
    }
}
