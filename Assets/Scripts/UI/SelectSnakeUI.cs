﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Core;
using Audio;

public enum SnakeTypes
{
    ALL, ENGINE, TIME_TRAVEL, BATTERING_RAM
}

namespace UI {

    public class SelectSnakeUI : MonoBehaviour
    {
        [SerializeField] GameObject[] snakesImages;
        [SerializeField] Button changeLeft, changeRight;
        [SerializeField] TextMeshProUGUI leftKeyText;
        [SerializeField] TextMeshProUGUI rightKeyText;

        int currentIndex;
        PlayerSetupUI playerSetupUI;

        #region Unity Functions

        private void OnEnable()
        {
            playerSetupUI = GetComponentInParent<PlayerSetupUI>();

            UpdateInfos();
        }

        #endregion

        #region Public Functions

        private void Update()
        {
            CycleThroughSnakes();
        }

        #endregion

        #region Private Functions

        void UpdateInfos()
        {
            leftKeyText.text = "Left: " + GameManager.Instance.FormatKey(playerSetupUI.leftKey);
            rightKeyText.text = "Right: " + GameManager.Instance.FormatKey(playerSetupUI.rightKey);

            playerSetupUI.SetPlayer((SnakeTypes)currentIndex);

            changeLeft.onClick.AddListener(() =>
            {
                AudioController.Instance.PlayAudio(Audio.AudioType.UI_BUTTON_CLICK);
            });

            changeRight.onClick.AddListener(() =>
            {
                AudioController.Instance.PlayAudio(Audio.AudioType.UI_BUTTON_CLICK);
            });

            TintImages();
        }

        void CycleThroughSnakes()
        {
            if (Input.GetKeyDown(playerSetupUI.leftKey))
            {
                CycleLeft();
            }
            else
            if (Input.GetKeyDown(playerSetupUI.rightKey))
            {
                CycleRight();
            }
        }

        public void CycleLeft()
        {
            snakesImages[currentIndex].gameObject.SetActive(false);

            currentIndex -= 1;

            if (currentIndex < 0)
            {
                currentIndex = snakesImages.Length - 1;
            }

            snakesImages[currentIndex].gameObject.SetActive(true);

            playerSetupUI.SetPlayer((SnakeTypes)currentIndex);

            AudioController.Instance.PlayAudio(Audio.AudioType.UI_BUTTON_CLICK);
        }

        public void CycleRight()
        {
            snakesImages[currentIndex].gameObject.SetActive(false);

            currentIndex += 1;

            if (currentIndex >= snakesImages.Length)
            {
                currentIndex = 0;
            }

            snakesImages[currentIndex].gameObject.SetActive(true);

            playerSetupUI.SetPlayer((SnakeTypes)currentIndex);

            AudioController.Instance.PlayAudio(Audio.AudioType.UI_BUTTON_CLICK);
        }

        void TintImages()
        {
            foreach(GameObject obj in snakesImages)
            {
                foreach (Image img in obj.GetComponentsInChildren<Image>())
                {
                    img.color = GameManager.Instance.colorScheme.GetColorByIndex(playerSetupUI.playerNumber - 1);
                }
            }

            changeLeft.GetComponent<Image>().color = GameManager.Instance.colorScheme.GetColorByIndex(playerSetupUI.playerNumber - 1);
            changeRight.GetComponent<Image>().color = GameManager.Instance.colorScheme.GetColorByIndex(playerSetupUI.playerNumber - 1); 
        }

        #endregion
    }
}
