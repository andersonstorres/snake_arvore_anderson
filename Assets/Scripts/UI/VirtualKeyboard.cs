﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Core;

namespace UI
{
    public class VirtualKeyboard : MonoBehaviour
    {
        public static VirtualKeyboard Instance;

        public List<VirtualKey> virtualKeyboardKeys = new List<VirtualKey>();

        [SerializeField] Transform keysHolder;
        [SerializeField] GameObject keyPrefab;
        [SerializeField] Color defaultColor;

        PlayerEnterUI playerEnterUI;


        [System.Serializable]
        public class VirtualKey
        {
            public KeyCode key;
            public GameObject virtualKey;

            public VirtualKey(KeyCode key, GameObject vKey)
            {
                this.key = key;
                virtualKey = vKey;
                virtualKey.GetComponentInChildren<TextMeshProUGUI>().text = GameManager.Instance.FormatKey(this.key);
            }
        }
        #region UnityFunctions

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            playerEnterUI = GetComponentInParent<PlayerEnterUI>();           
            CreateVirtualKeyboard();
        }

        #endregion

        #region Public Functions

        public void TintKey(KeyCode key, Color color)
        {
            foreach(VirtualKey vk in virtualKeyboardKeys)
            {
                if(vk.key == key)
                {
                    vk.virtualKey.GetComponent<Image>().color = color;
                }
            }
        }

        #endregion

        #region Private Functions

        void CreateVirtualKeyboard()
        {
            List<KeyCode> tempKeys = playerEnterUI.CopyKeyList();
            for(int i = 0; i < tempKeys.Count; i++)
            {
                GameObject keyObject = Instantiate(keyPrefab, keysHolder);
                virtualKeyboardKeys.Add(new VirtualKey(tempKeys[i], keyObject));
            }
        }

        #endregion
    }
}
