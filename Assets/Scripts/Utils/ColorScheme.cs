﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColorName
{
    C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11, C12, C13, C14, C15, C16, C17, C18
}

namespace Utils
{
    [CreateAssetMenu(fileName = "Color Scheme", menuName = "New Color Scheme")]
    public class ColorScheme : ScriptableObject
    {
        public static ColorScheme Instance;

        [SerializeField] List<ColorPreset> colors = new List<ColorPreset>();

        [System.Serializable]
        public class ColorPreset
        {
            public ColorName name;
            public Color color;
            public Material colorMaterial;
        }

        private void Awake()
        {
            Instance = this;
        }

        public Color GetColorByIndex(int index)
        {
            return colors[index].color;
        }

        public Material GetMaterialByIndex(int index)
        {
            return colors[index].colorMaterial;
        }

        public Color GetColorByName(ColorName colorName)
        {
            foreach (ColorPreset c_preset in colors)
            {
                if (c_preset.name == colorName)
                {
                    return c_preset.color;
                }
            }

            return Color.magenta;
        }

        public Material GetMaterialByName(ColorName colorName)
        {
            foreach (ColorPreset c_preset in colors)
            {
                if (c_preset.name == colorName)
                {
                    return c_preset.colorMaterial;
                }
            }

            return null;
        }

        public int GetColorCount()
        {
            return colors.Count;
        }
    }
}
