﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugHelper : MonoBehaviour
{
    #region Singleton
    private static DebugHelper _instance;

    public static DebugHelper Instance { get { return _instance; } }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }
    #endregion

    public bool debugOn;
    public bool debugPath;

    public void Log(string _msg, GameObject obj)
    {
        if (!debugOn) return;
        Debug.Log(obj.name + ": " + _msg);
    }

    public void LogWarning(string _msg, GameObject obj)
    {
        if (!debugOn) return;
        Debug.LogWarning(obj.name + ": " + _msg);
    }

    public void LogError(string _msg, GameObject obj)
    {
        if (!debugOn) return;
        Debug.LogError(obj + ": " + _msg);
    }
}
