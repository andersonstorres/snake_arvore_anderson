﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kino;

public class EffectsHelper : MonoBehaviour
{
    public static EffectsHelper Instance;

    [Header("VFX")]
    public GameObject hitBasic;
    public GameObject electricExplosion;
    public GameObject spawnBlockEffects;

    [Header("Block Punch")]
    public float p_scale = 1.05f;
    public float p_duration = 0.4f;

    [Header("Block Grown")]
    public float growDuration = 0.8f;

    [Header("Camera Shake")]
    public float c_duration = 0.5f;
    public float c_strength = 0.2f;
    [HideInInspector] public bool c_shaking;

    [Header("Time Travel")]
    public AnalogGlitch rewindEffect;
    public GameObject rewindUI;

    private void Awake()
    {
        Instance = this;
    }
}
